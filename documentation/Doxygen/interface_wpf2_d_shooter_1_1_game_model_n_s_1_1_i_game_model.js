var interface_wpf2_d_shooter_1_1_game_model_n_s_1_1_i_game_model =
[
    [ "Bomb", "interface_wpf2_d_shooter_1_1_game_model_n_s_1_1_i_game_model.html#a419184925864bacd5b8be495127004b9", null ],
    [ "Bots", "interface_wpf2_d_shooter_1_1_game_model_n_s_1_1_i_game_model.html#ad93d67b8fbf0340393bc305c3d6f1f58", null ],
    [ "Bullets", "interface_wpf2_d_shooter_1_1_game_model_n_s_1_1_i_game_model.html#a47a0d1703801324ec5ecc334e0ca05e0", null ],
    [ "ChosenTeam", "interface_wpf2_d_shooter_1_1_game_model_n_s_1_1_i_game_model.html#add7956664e90b508d85a50f637949ccc", null ],
    [ "FreezeTime", "interface_wpf2_d_shooter_1_1_game_model_n_s_1_1_i_game_model.html#a335dbe06b0946d1e861479632e8c2317", null ],
    [ "GameHeight", "interface_wpf2_d_shooter_1_1_game_model_n_s_1_1_i_game_model.html#a6f366283c0ccf3cbaa68648847aa3045", null ],
    [ "GameWidth", "interface_wpf2_d_shooter_1_1_game_model_n_s_1_1_i_game_model.html#a7e4be55e38861f3a2d7f24a7c95991ec", null ],
    [ "Map", "interface_wpf2_d_shooter_1_1_game_model_n_s_1_1_i_game_model.html#a8ab981bb8c2f5fe08fbabc6cf712acce", null ],
    [ "NumberOfWonRoundsByCounterTerrorists", "interface_wpf2_d_shooter_1_1_game_model_n_s_1_1_i_game_model.html#af4ae79703394b2d64bc91b751ba20bcb", null ],
    [ "NumberOfWonRoundsByTerrorists", "interface_wpf2_d_shooter_1_1_game_model_n_s_1_1_i_game_model.html#a02506073d72857e8b3e941dca87b41f8", null ],
    [ "Player", "interface_wpf2_d_shooter_1_1_game_model_n_s_1_1_i_game_model.html#ababf21ba5597663c458f17db0204bba9", null ],
    [ "RoundTime", "interface_wpf2_d_shooter_1_1_game_model_n_s_1_1_i_game_model.html#a0b54c66118efe59c2a79ec201ef6addb", null ],
    [ "TileSizeHeight", "interface_wpf2_d_shooter_1_1_game_model_n_s_1_1_i_game_model.html#ac3fd930432a9e485d00c16cad726b587", null ],
    [ "TileSizeWidth", "interface_wpf2_d_shooter_1_1_game_model_n_s_1_1_i_game_model.html#a63643fa71d2ee6e751f8bd2e529f3ea0", null ]
];