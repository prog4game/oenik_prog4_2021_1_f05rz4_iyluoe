var interface_wpf2_d_shooter_1_1_game_model_n_s_1_1_i_weapon =
[
    [ "CanShoot", "interface_wpf2_d_shooter_1_1_game_model_n_s_1_1_i_weapon.html#ab83966a984cd7e914279fde9bf168663", null ],
    [ "AmmoCapacity", "interface_wpf2_d_shooter_1_1_game_model_n_s_1_1_i_weapon.html#a1ed0f0cfcb5c8027809dc7761657b8a8", null ],
    [ "BulletVelocity", "interface_wpf2_d_shooter_1_1_game_model_n_s_1_1_i_weapon.html#a2655a0d9fc86de51c2a804330adfcf21", null ],
    [ "CoolDown", "interface_wpf2_d_shooter_1_1_game_model_n_s_1_1_i_weapon.html#a554b63ea8d31c44511280789dcdef197", null ],
    [ "Damage", "interface_wpf2_d_shooter_1_1_game_model_n_s_1_1_i_weapon.html#ad59c535492a91c417440ee581bf6d5dc", null ],
    [ "Price", "interface_wpf2_d_shooter_1_1_game_model_n_s_1_1_i_weapon.html#ae717ec7313eee7e2f10d03f94d61e818", null ],
    [ "WeaponType", "interface_wpf2_d_shooter_1_1_game_model_n_s_1_1_i_weapon.html#aa7d4522256b2e7d58768bf649d3515af", null ]
];