var hierarchy =
[
    [ "Application", null, [
      [ "Wpf2DShooter.App", "class_wpf2_d_shooter_1_1_app.html", null ]
    ] ],
    [ "EventArgs", null, [
      [ "Wpf2DShooter.GameLogicNS.GameSoundEffectsEventArgs", "class_wpf2_d_shooter_1_1_game_logic_n_s_1_1_game_sound_effects_event_args.html", null ]
    ] ],
    [ "FrameworkElement", null, [
      [ "Wpf2DShooter.GameControlWindow.GameControl", "class_wpf2_d_shooter_1_1_game_control_window_1_1_game_control.html", null ]
    ] ],
    [ "Wpf2DShooter.GameRendererNS.GameRenderer", "class_wpf2_d_shooter_1_1_game_renderer_n_s_1_1_game_renderer.html", null ],
    [ "IDisposable", null, [
      [ "Wpf2DShooter.GameControlWindow.GameControl", "class_wpf2_d_shooter_1_1_game_control_window_1_1_game_control.html", null ],
      [ "Wpf2DShooter.GameLogicNS.IGameLogic", "interface_wpf2_d_shooter_1_1_game_logic_n_s_1_1_i_game_logic.html", [
        [ "Wpf2DShooter.GameLogicNS.GameLogic", "class_wpf2_d_shooter_1_1_game_logic_n_s_1_1_game_logic.html", null ]
      ] ]
    ] ],
    [ "Wpf2DShooter.GameModelNS.IGameModel", "interface_wpf2_d_shooter_1_1_game_model_n_s_1_1_i_game_model.html", [
      [ "Wpf2DShooter.GameModelNS.GameModel", "class_wpf2_d_shooter_1_1_game_model_n_s_1_1_game_model.html", null ]
    ] ],
    [ "Wpf2DShooter.GameModelNS.IMovable", "interface_wpf2_d_shooter_1_1_game_model_n_s_1_1_i_movable.html", [
      [ "Wpf2DShooter.GameModelNS.IBomb", "interface_wpf2_d_shooter_1_1_game_model_n_s_1_1_i_bomb.html", [
        [ "Wpf2DShooter.GameModelNS.Bomb", "class_wpf2_d_shooter_1_1_game_model_n_s_1_1_bomb.html", null ]
      ] ],
      [ "Wpf2DShooter.GameModelNS.IBullet", "interface_wpf2_d_shooter_1_1_game_model_n_s_1_1_i_bullet.html", [
        [ "Wpf2DShooter.GameModelNS.Bullet", "class_wpf2_d_shooter_1_1_game_model_n_s_1_1_bullet.html", null ]
      ] ],
      [ "Wpf2DShooter.GameModelNS.IEntity", "interface_wpf2_d_shooter_1_1_game_model_n_s_1_1_i_entity.html", [
        [ "Wpf2DShooter.GameModelNS.Entity", "class_wpf2_d_shooter_1_1_game_model_n_s_1_1_entity.html", null ]
      ] ]
    ] ],
    [ "InternalTypeHelper", null, [
      [ "XamlGeneratedNamespace.GeneratedInternalTypeHelper", "class_xaml_generated_namespace_1_1_generated_internal_type_helper.html", null ]
    ] ],
    [ "Wpf2DShooter.GameModelNS.IWeapon", "interface_wpf2_d_shooter_1_1_game_model_n_s_1_1_i_weapon.html", [
      [ "Wpf2DShooter.GameModelNS.Weapon", "class_wpf2_d_shooter_1_1_game_model_n_s_1_1_weapon.html", null ]
    ] ],
    [ "Window", null, [
      [ "Wpf2DShooter.GameControlWindow.ChoseSideWindow", "class_wpf2_d_shooter_1_1_game_control_window_1_1_chose_side_window.html", null ],
      [ "Wpf2DShooter.GameControlWindow.MainMenuWindow", "class_wpf2_d_shooter_1_1_game_control_window_1_1_main_menu_window.html", null ],
      [ "Wpf2DShooter.MainWindow", "class_wpf2_d_shooter_1_1_main_window.html", null ]
    ] ]
];