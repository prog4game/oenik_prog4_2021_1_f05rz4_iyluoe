var class_wpf2_d_shooter_1_1_game_model_n_s_1_1_bullet =
[
    [ "Bullet", "class_wpf2_d_shooter_1_1_game_model_n_s_1_1_bullet.html#ab41ce9c9da55e0f3b6f58c13b463b935", null ],
    [ "ChangeCoordinatesBySpeed", "class_wpf2_d_shooter_1_1_game_model_n_s_1_1_bullet.html#a25763029733f38b1e712193863645192", null ],
    [ "Angle", "class_wpf2_d_shooter_1_1_game_model_n_s_1_1_bullet.html#a10cd5f66375641fcf79542268850bae9", null ],
    [ "Damage", "class_wpf2_d_shooter_1_1_game_model_n_s_1_1_bullet.html#a50a45928fd18ecb054509cbf9504c7ad", null ],
    [ "DX", "class_wpf2_d_shooter_1_1_game_model_n_s_1_1_bullet.html#afec1ad88180eb8c24d306f61ce20cf08", null ],
    [ "DY", "class_wpf2_d_shooter_1_1_game_model_n_s_1_1_bullet.html#adee120ed75c2f752476d6427806e2027", null ],
    [ "Owner", "class_wpf2_d_shooter_1_1_game_model_n_s_1_1_bullet.html#a07fee319b0c670960f26af06bf5bebdb", null ],
    [ "WasLethal", "class_wpf2_d_shooter_1_1_game_model_n_s_1_1_bullet.html#afe9e6071615f5d12c90f9e7748f39782", null ],
    [ "X", "class_wpf2_d_shooter_1_1_game_model_n_s_1_1_bullet.html#a80f120c8312e2b95caa541da78bd2a9d", null ],
    [ "Y", "class_wpf2_d_shooter_1_1_game_model_n_s_1_1_bullet.html#a72ffec6cd6846552df376216db486bfb", null ]
];