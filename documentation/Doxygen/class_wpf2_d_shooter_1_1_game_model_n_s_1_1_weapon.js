var class_wpf2_d_shooter_1_1_game_model_n_s_1_1_weapon =
[
    [ "CanShoot", "class_wpf2_d_shooter_1_1_game_model_n_s_1_1_weapon.html#a9426317578d1cb9c7970c0a2ef676c4b", null ],
    [ "AmmoCapacity", "class_wpf2_d_shooter_1_1_game_model_n_s_1_1_weapon.html#a09d1bd0ab5c49f5bbcc21234045939de", null ],
    [ "BulletVelocity", "class_wpf2_d_shooter_1_1_game_model_n_s_1_1_weapon.html#aed5ee3054d11852f4b4e19961f2c0d02", null ],
    [ "CoolDown", "class_wpf2_d_shooter_1_1_game_model_n_s_1_1_weapon.html#a981e233dc10a68540e42650280ab120a", null ],
    [ "Damage", "class_wpf2_d_shooter_1_1_game_model_n_s_1_1_weapon.html#a538a08a360ad27ebcf9ab94f967a8832", null ],
    [ "Price", "class_wpf2_d_shooter_1_1_game_model_n_s_1_1_weapon.html#a1a83568d67309b897c08fca78951e5bd", null ],
    [ "WeaponType", "class_wpf2_d_shooter_1_1_game_model_n_s_1_1_weapon.html#a2d295445b72ab0370408019d1099ba9e", null ]
];