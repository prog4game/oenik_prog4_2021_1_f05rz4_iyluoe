var interface_wpf2_d_shooter_1_1_game_model_n_s_1_1_i_entity =
[
    [ "HasBomb", "interface_wpf2_d_shooter_1_1_game_model_n_s_1_1_i_entity.html#a49c26d61d55882d0a4522bee50ce6103", null ],
    [ "HasBombInHands", "interface_wpf2_d_shooter_1_1_game_model_n_s_1_1_i_entity.html#ad4100693993375bec492f2eb36d3124e", null ],
    [ "Health", "interface_wpf2_d_shooter_1_1_game_model_n_s_1_1_i_entity.html#ad51f0053021f90620d5b13c37bf1bf6c", null ],
    [ "Model", "interface_wpf2_d_shooter_1_1_game_model_n_s_1_1_i_entity.html#af2d898b0097d80721f1000061d35e1e5", null ],
    [ "Team", "interface_wpf2_d_shooter_1_1_game_model_n_s_1_1_i_entity.html#a9ded37d5e770667798c38aaabcb03936", null ],
    [ "Weapon", "interface_wpf2_d_shooter_1_1_game_model_n_s_1_1_i_entity.html#acd31c5964e48b96dff98e99682eff0c4", null ]
];