var searchData=
[
  ['team_279',['Team',['../class_wpf2_d_shooter_1_1_game_model_n_s_1_1_entity.html#ac9a5b57072742771c0c60f292539c82b',1,'Wpf2DShooter.GameModelNS.Entity.Team()'],['../interface_wpf2_d_shooter_1_1_game_model_n_s_1_1_i_entity.html#a9ded37d5e770667798c38aaabcb03936',1,'Wpf2DShooter.GameModelNS.IEntity.Team()']]],
  ['tilesizeheight_280',['TileSizeHeight',['../class_wpf2_d_shooter_1_1_game_model_n_s_1_1_game_model.html#a639d524870df489b87c4d9806f7b980d',1,'Wpf2DShooter.GameModelNS.GameModel.TileSizeHeight()'],['../interface_wpf2_d_shooter_1_1_game_model_n_s_1_1_i_game_model.html#ac3fd930432a9e485d00c16cad726b587',1,'Wpf2DShooter.GameModelNS.IGameModel.TileSizeHeight()']]],
  ['tilesizewidth_281',['TileSizeWidth',['../class_wpf2_d_shooter_1_1_game_model_n_s_1_1_game_model.html#a2acb6878e360424bffd69eb49ba83f5a',1,'Wpf2DShooter.GameModelNS.GameModel.TileSizeWidth()'],['../interface_wpf2_d_shooter_1_1_game_model_n_s_1_1_i_game_model.html#a63643fa71d2ee6e751f8bd2e529f3ea0',1,'Wpf2DShooter.GameModelNS.IGameModel.TileSizeWidth()']]]
];
