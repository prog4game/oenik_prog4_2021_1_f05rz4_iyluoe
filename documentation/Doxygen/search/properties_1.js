var searchData=
[
  ['bomb_247',['Bomb',['../class_wpf2_d_shooter_1_1_game_model_n_s_1_1_game_model.html#a94f328ce16fba96227fcff4e9652efd7',1,'Wpf2DShooter.GameModelNS.GameModel.Bomb()'],['../interface_wpf2_d_shooter_1_1_game_model_n_s_1_1_i_game_model.html#a419184925864bacd5b8be495127004b9',1,'Wpf2DShooter.GameModelNS.IGameModel.Bomb()']]],
  ['bots_248',['Bots',['../class_wpf2_d_shooter_1_1_game_model_n_s_1_1_game_model.html#a4ff8012fd0babdcde4cad50761cfbdc1',1,'Wpf2DShooter.GameModelNS.GameModel.Bots()'],['../interface_wpf2_d_shooter_1_1_game_model_n_s_1_1_i_game_model.html#ad93d67b8fbf0340393bc305c3d6f1f58',1,'Wpf2DShooter.GameModelNS.IGameModel.Bots()']]],
  ['bullets_249',['Bullets',['../class_wpf2_d_shooter_1_1_game_model_n_s_1_1_game_model.html#a24939cc0565702b7f7500fd634bfb9c1',1,'Wpf2DShooter.GameModelNS.GameModel.Bullets()'],['../interface_wpf2_d_shooter_1_1_game_model_n_s_1_1_i_game_model.html#a47a0d1703801324ec5ecc334e0ca05e0',1,'Wpf2DShooter.GameModelNS.IGameModel.Bullets()']]],
  ['bulletvelocity_250',['BulletVelocity',['../class_wpf2_d_shooter_1_1_game_model_n_s_1_1_weapon.html#aed5ee3054d11852f4b4e19961f2c0d02',1,'Wpf2DShooter.GameModelNS.Weapon.BulletVelocity()'],['../interface_wpf2_d_shooter_1_1_game_model_n_s_1_1_i_weapon.html#a2655a0d9fc86de51c2a804330adfcf21',1,'Wpf2DShooter.GameModelNS.IWeapon.BulletVelocity()']]]
];
