var searchData=
[
  ['ibomb_148',['IBomb',['../interface_wpf2_d_shooter_1_1_game_model_n_s_1_1_i_bomb.html',1,'Wpf2DShooter::GameModelNS']]],
  ['ibullet_149',['IBullet',['../interface_wpf2_d_shooter_1_1_game_model_n_s_1_1_i_bullet.html',1,'Wpf2DShooter::GameModelNS']]],
  ['ientity_150',['IEntity',['../interface_wpf2_d_shooter_1_1_game_model_n_s_1_1_i_entity.html',1,'Wpf2DShooter::GameModelNS']]],
  ['igamelogic_151',['IGameLogic',['../interface_wpf2_d_shooter_1_1_game_logic_n_s_1_1_i_game_logic.html',1,'Wpf2DShooter::GameLogicNS']]],
  ['igamemodel_152',['IGameModel',['../interface_wpf2_d_shooter_1_1_game_model_n_s_1_1_i_game_model.html',1,'Wpf2DShooter::GameModelNS']]],
  ['imovable_153',['IMovable',['../interface_wpf2_d_shooter_1_1_game_model_n_s_1_1_i_movable.html',1,'Wpf2DShooter::GameModelNS']]],
  ['iweapon_154',['IWeapon',['../interface_wpf2_d_shooter_1_1_game_model_n_s_1_1_i_weapon.html',1,'Wpf2DShooter::GameModelNS']]]
];
