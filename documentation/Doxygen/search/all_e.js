var searchData=
[
  ['player_98',['Player',['../class_wpf2_d_shooter_1_1_game_model_n_s_1_1_game_model.html#a84f609e621940d09fb05ceee13e196e6',1,'Wpf2DShooter.GameModelNS.GameModel.Player()'],['../interface_wpf2_d_shooter_1_1_game_model_n_s_1_1_i_game_model.html#ababf21ba5597663c458f17db0204bba9',1,'Wpf2DShooter.GameModelNS.IGameModel.Player()']]],
  ['positionofentityonscreen_99',['PositionOfEntityOnScreen',['../class_wpf2_d_shooter_1_1_game_logic_n_s_1_1_game_logic.html#a26b6994b0fb58a7242a291ed455cd00d',1,'Wpf2DShooter.GameLogicNS.GameLogic.PositionOfEntityOnScreen()'],['../interface_wpf2_d_shooter_1_1_game_logic_n_s_1_1_i_game_logic.html#ab2a143e7f0222b9384be59c5ba61397b',1,'Wpf2DShooter.GameLogicNS.IGameLogic.PositionOfEntityOnScreen()']]],
  ['price_100',['Price',['../class_wpf2_d_shooter_1_1_game_model_n_s_1_1_weapon.html#a1a83568d67309b897c08fca78951e5bd',1,'Wpf2DShooter.GameModelNS.Weapon.Price()'],['../interface_wpf2_d_shooter_1_1_game_model_n_s_1_1_i_weapon.html#ae717ec7313eee7e2f10d03f94d61e818',1,'Wpf2DShooter.GameModelNS.IWeapon.Price()']]]
];
