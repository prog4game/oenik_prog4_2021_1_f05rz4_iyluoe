var indexSectionsWithContent =
{
  0: "abcdefghijlmnoprstuwxy",
  1: "abcegimw",
  2: "wx",
  3: "abcdfghimopsu",
  4: "w",
  5: "dmnstw",
  6: "bcdglnrtuw",
  7: "abcdfghijmnoprstwxy"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "namespaces",
  3: "functions",
  4: "variables",
  5: "enums",
  6: "enumvalues",
  7: "properties"
};

var indexSectionLabels =
{
  0: "All",
  1: "Classes",
  2: "Namespaces",
  3: "Functions",
  4: "Variables",
  5: "Enumerations",
  6: "Enumerator",
  7: "Properties"
};

