var class_wpf2_d_shooter_1_1_game_model_n_s_1_1_entity =
[
    [ "Angle", "class_wpf2_d_shooter_1_1_game_model_n_s_1_1_entity.html#a691f1b20ffea4b43129d3ffc01ac03aa", null ],
    [ "DX", "class_wpf2_d_shooter_1_1_game_model_n_s_1_1_entity.html#a60604e5521edfc99c4635e5674c5c9c1", null ],
    [ "DY", "class_wpf2_d_shooter_1_1_game_model_n_s_1_1_entity.html#a2c7f5d1f78b1c1321724a57f9d4abe07", null ],
    [ "HasBomb", "class_wpf2_d_shooter_1_1_game_model_n_s_1_1_entity.html#abeb85f2c6413152a278f8a054de8031d", null ],
    [ "HasBombInHands", "class_wpf2_d_shooter_1_1_game_model_n_s_1_1_entity.html#a5db0541f7815b13a161e92caa7e42c63", null ],
    [ "Health", "class_wpf2_d_shooter_1_1_game_model_n_s_1_1_entity.html#a1a11fab1520f28bf8d4d2e095c41eb31", null ],
    [ "Model", "class_wpf2_d_shooter_1_1_game_model_n_s_1_1_entity.html#a4cac0aaaf9862ad9bb83a1f7199dad60", null ],
    [ "Team", "class_wpf2_d_shooter_1_1_game_model_n_s_1_1_entity.html#ac9a5b57072742771c0c60f292539c82b", null ],
    [ "Weapon", "class_wpf2_d_shooter_1_1_game_model_n_s_1_1_entity.html#a738c7c508a866b533e3af4a4df8462a3", null ],
    [ "X", "class_wpf2_d_shooter_1_1_game_model_n_s_1_1_entity.html#aa3d252a8954cb64da9a8b9680959f09d", null ],
    [ "Y", "class_wpf2_d_shooter_1_1_game_model_n_s_1_1_entity.html#ad483bce12e2ed4d84a28d4982a10c673", null ]
];