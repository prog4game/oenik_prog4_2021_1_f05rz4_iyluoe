﻿// <copyright file="BulletDrawing.cs" company="Klavaro">
// Copyright (c) Klavaro. All rights reserved.
// </copyright>

namespace Wpf2DShooter.GameRendererNS
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Media;

    /// <summary>
    /// Helper class for the <see cref="GameRenderer"/> class by discribing data about a bullet drawing.
    /// </summary>
    internal class BulletDrawing
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="BulletDrawing"/> class.
        /// </summary>
        public BulletDrawing()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="BulletDrawing"/> class.
        /// </summary>
        /// <param name="bulletPos">The point where the bullet is on the screen.</param>
        /// <param name="geometryDrawing">A copy of <see cref="GeometryDrawing"/>.</param>
        /// <param name="opacity">Value representing the opacity of the drawing.</param>
        public BulletDrawing(Point bulletPos, GeometryDrawing geometryDrawing, byte opacity)
        {
            this.BulletPos = bulletPos;
            this.Drawing = geometryDrawing;
            this.Opacity = opacity;
        }

        /// <summary>
        /// Gets : the <see cref="GeometryDrawing"/> of the bullet.
        /// </summary>
        public GeometryDrawing Drawing { get; private set; }

        /// <summary>
        /// Gets or sets : the value representing the opacity of the drawing.
        /// </summary>
        public byte Opacity { get; set; }

        /// <summary>
        /// Gets : the point where the bullet is on the screen.
        /// </summary>
        public Point BulletPos { get; private set; }
    }
}
