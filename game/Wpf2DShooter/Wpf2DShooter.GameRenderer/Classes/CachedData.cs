﻿// <copyright file="CachedData.cs" company="Klavaro">
// Copyright (c) Klavaro. All rights reserved.
// </copyright>

namespace Wpf2DShooter.GameRendererNS
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows.Media;

    /// <summary>
    /// Helper class for cached data about the entities.
    /// </summary>
    internal class CachedData
    {
        /// <summary>
        /// Gets or Sets : The cached drawing of the entity.
        /// </summary>
        public Drawing Drawing { get; set; }

        /// <summary>
        /// Gets or Sets : The cached X coordinate of the entity.
        /// </summary>
        public double X { get; set; }

        /// <summary>
        /// Gets or Sets : The cached Y coordinate of the entity.
        /// </summary>
        public double Y { get; set; }

        /// <summary>
        /// Gets or Sets : The angle of the entity.
        /// </summary>
        public double Angle { get; set; }
    }
}
