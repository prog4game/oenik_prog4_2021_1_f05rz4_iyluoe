﻿// <copyright file="ViewboxType.cs" company="Klavaro">
// Copyright (c) Klavaro. All rights reserved.
// </copyright>

namespace Wpf2DShooter.GameRendererNS
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Describes the different type of offsets for the Entity brush.
    /// </summary>
    internal enum ViewboxType
    {
        /// <summary>
        /// Default option for images with only one part.
        /// </summary>
        Default,

        /// <summary>
        /// Offset for the Rifle holding part of the image for the Entity brush.
        /// </summary>
        Rifle,

        /// <summary>
        /// Offset for the Reloading part of the image for the Entity brush.
        /// </summary>
        Reload,

        /// <summary>
        /// Offset for the Pistol holding part of the image for the Entity brush.
        /// </summary>
        Pistol,

        /// <summary>
        /// Offset for the ShotBlood part of the image for the Blood brush.
        /// </summary>
        ShotBlood,

        /// <summary>
        /// Offset for the PuddleBlood part of the image for the Blood brush.
        /// </summary>
        PuddleBlood,
    }
}
