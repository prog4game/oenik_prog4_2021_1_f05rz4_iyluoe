﻿// <copyright file="GameRenderer.cs" company="Klavaro">
// Copyright (c) Klavaro. All rights reserved.
// </copyright>

namespace Wpf2DShooter.GameRendererNS
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Media;
    using System.Windows.Media.Imaging;
    using Wpf2DShooter.GameLogicNS;
    using Wpf2DShooter.GameModelNS;

    /// <summary>
    /// Does the rendering for the game.
    /// </summary>
    public class GameRenderer
    {
        private static readonly Random Rnd = new Random();

        private IGameLogic logic;

        private BrushManager brushManager;

        private Drawing cachedBomb;
        private DrawingGroup cachedMap;
        private List<BulletDrawing> cachedBullets;
        private CachedData[] cachedEntities;
        private Point center;
        private Point lastPoint;

        /// <summary>
        /// Initializes a new instance of the <see cref="GameRenderer"/> class.
        /// </summary>
        /// <param name="logic">An <see cref="IGameLogic"/> implementation to work with.</param>
        public GameRenderer(IGameLogic logic)
        {
            this.logic = logic;

            this.brushManager = new BrushManager(this.logic);

            this.cachedEntities = new CachedData[this.logic.Model.Bots.Count + 1];
        }

        /// <summary>
        /// Calculates the center of the screen.
        /// </summary>
        /// <returns>The center of the screen.</returns>
        public Point CenterOfScreen()
        {
            if (this.center.Equals(default(Point)))
            {
                this.center = new Point(
                    (this.logic.Model.GameWidth - this.logic.Model.TileSizeWidth) / 2,
                    (this.logic.Model.GameHeight - this.logic.Model.TileSizeHeight) / 2);
            }

            return this.center;
        }

        /// <summary>
        /// Resets the Game Renderer.
        /// </summary>
        public void ClearCache()
        {
            this.cachedBomb = null;
            this.cachedMap = null;
            this.cachedBullets = null;
            this.cachedEntities = null;
            this.center = default(Point);
        }

        /// <summary>
        /// Clears the bullets and blood.
        /// </summary>
        public void ClearBullets()
        {
            this.cachedBullets = null;
        }

        /// <summary>
        /// Builds the needed drawings into one <see cref="DrawingGroup"/>.
        /// </summary>
        /// <returns>A <see cref="DrawingGroup"/>.</returns>
        public Drawing BuildDrawing()
        {
            DrawingGroup drawingGroup = this.GenerateMap();
            drawingGroup.Children.Add(this.GetBomb());
            drawingGroup.Children.Add(this.GetBullets());
            drawingGroup.Children.Add(this.GetEntities());

            this.CenterDrawingForPlayer(drawingGroup);
            return drawingGroup;
        }

        private Drawing GetHudInfo(IEntity entity)
        {
            DrawingGroup drawingGroup = new DrawingGroup();

            string printString = $"CT: {this.logic.Model.NumberOfWonRoundsByCounterTerrorists} - T: {this.logic.Model.NumberOfWonRoundsByTerrorists}";

            FormattedText wave = new FormattedText(
                printString,
                System.Globalization.CultureInfo.CurrentCulture,
                FlowDirection.LeftToRight,
                new Typeface("Arial"),
                25,
                Brushes.LimeGreen,
                1.0);

            Point drawingPoint = this.lastPoint;
            Point center = this.CenterOfScreen();
            drawingPoint.Y -= center.Y - 40;
            drawingPoint.X -= 10;

            GeometryDrawing waveDrawing = new GeometryDrawing(
                Brushes.LimeGreen,
                new Pen(Brushes.LimeGreen, 1),
                wave.BuildGeometry(drawingPoint));
            drawingGroup.Children.Add(waveDrawing);

            var time = this.logic.Model.RoundTime;

            printString = $"{(int)time.TotalMinutes}:{time.Seconds:00}";

            wave = new FormattedText(
                printString,
                System.Globalization.CultureInfo.CurrentCulture,
                FlowDirection.LeftToRight,
                new Typeface("Arial"),
                25,
                Brushes.LimeGreen,
                1.0);

            drawingPoint.Y -= 35;
            drawingPoint.X += 40;

            waveDrawing = new GeometryDrawing(
                Brushes.LimeGreen,
                new Pen(Brushes.LimeGreen, 1),
                wave.BuildGeometry(drawingPoint));
            drawingGroup.Children.Add(waveDrawing);

            if (entity != null)
            {
                printString = $"HP: {entity.Health}";

                wave = new FormattedText(
                    printString,
                    System.Globalization.CultureInfo.CurrentCulture,
                    FlowDirection.LeftToRight,
                    new Typeface("Arial"),
                    25,
                    Brushes.LimeGreen,
                    1.0);

                drawingPoint.Y = this.lastPoint.Y + center.Y + 30;
                drawingPoint.X = this.lastPoint.X - center.X + 20;

                waveDrawing = new GeometryDrawing(
                    Brushes.LimeGreen,
                    new Pen(Brushes.LimeGreen, 1),
                    wave.BuildGeometry(drawingPoint));
                drawingGroup.Children.Add(waveDrawing);
            }

            return drawingGroup;
        }

        private Drawing GetBomb()
        {
            if (!this.logic.Model.Bomb.IsPlanted)
            {
                return new GeometryDrawing();
            }

            if (this.cachedBomb == null)
            {
                BrushClass bombBrush = this.brushManager.BombBrush();

                TransformGroup transformGroup = new TransformGroup();
                {
                    ScaleTransform scaleTransform = new ScaleTransform(
                        this.logic.Model.TileSizeWidth / bombBrush.MaxSize,
                        this.logic.Model.TileSizeHeight / bombBrush.MaxSize,
                        0.5,
                        0.5);

                    transformGroup.Children.Add(scaleTransform);

                    TranslateTransform translateTransform = new TranslateTransform();
                    Point bombPos = this.logic.PositionOfEntityOnScreen(this.logic.Model.Bomb);
                    translateTransform.X = bombPos.X;
                    translateTransform.Y = bombPos.Y;

                    transformGroup.Children.Add(translateTransform);
                }

                Geometry geometry = new RectangleGeometry(new Rect(0, 0, bombBrush.MaxSize, bombBrush.MaxSize), 0, 0, transformGroup);

                RotateTransform rotateTransform = new RotateTransform
                {
                    CenterX = 0.5,
                    CenterY = 0.5,
                    Angle = this.logic.Model.Bomb.Angle,
                };
                bombBrush.Brush.RelativeTransform = rotateTransform;

                this.cachedBomb = new GeometryDrawing(bombBrush.Brush, null, geometry);
            }

            return this.cachedBomb;
        }

        private Drawing GetBullets()
        {
            DrawingGroup drawingGroup = new DrawingGroup();

            if (this.cachedBullets == null)
            {
                this.cachedBullets = new List<BulletDrawing>();
            }

            this.logic.HandleBulletsAndReturnListOfBulletsThatHitSomething(
                out List<IBullet> wallBullets,
                out List<IBullet> entityBullets);

            if (wallBullets.Count > 0)
            {
                BrushClass bulletHoleBrush = this.brushManager.BulletHoleBrush();

                foreach (IBullet bullet in wallBullets)
                {
                    bullet.DX /= 3;
                    bullet.DY /= 3;
                    bullet.ChangeCoordinatesBySpeed();

                    Point bulletPos = this.logic.PositionOfEntityOnScreen(bullet);

                    var alreadyHitBullet = this.cachedBullets.FirstOrDefault(x => x.BulletPos.Equals(bulletPos));
                    if (alreadyHitBullet != null)
                    {
                        alreadyHitBullet.Opacity = 255;
                        continue;
                    }

                    TransformGroup transformGroup = new TransformGroup();
                    {
                        ScaleTransform scaleTransform = new ScaleTransform(
                            this.logic.Model.TileSizeWidth * Config.BulletHoleWidth / bulletHoleBrush.MaxSize,
                            this.logic.Model.TileSizeHeight * Config.BulletHoleHeight / bulletHoleBrush.MaxSize,
                            0.5,
                            0.5);

                        transformGroup.Children.Add(scaleTransform);

                        RotateTransform rotateTransform = new RotateTransform
                        {
                            CenterX = 0.5,
                            CenterY = 0.5,
                            Angle = bullet.Angle,
                        };

                        transformGroup.Children.Add(rotateTransform);
                        bulletHoleBrush.Brush.RelativeTransform = rotateTransform;

                        TranslateTransform translateTransform = new TranslateTransform();

                        translateTransform.X = bulletPos.X;
                        translateTransform.Y = bulletPos.Y;

                        transformGroup.Children.Add(translateTransform);
                    }

                    Geometry geometry = new RectangleGeometry(new Rect(0, 0, bulletHoleBrush.MaxSize, bulletHoleBrush.MaxSize), 0, 0, transformGroup);

                    this.cachedBullets.Add(new BulletDrawing(bulletPos, new GeometryDrawing(bulletHoleBrush.Brush.Clone(), null, geometry), 255));
                }
            }

            if (entityBullets.Count > 0)
            {
                foreach (IBullet bullet in entityBullets)
                {
                    BrushClass bloodBrush;
                    if (bullet.WasLethal)
                    {
                        bloodBrush = this.brushManager.BloodBrush(ViewboxType.PuddleBlood, (byte)Rnd.Next(3, 5));
                    }
                    else
                    {
                        bloodBrush = this.brushManager.BloodBrush(ViewboxType.ShotBlood, (byte)Rnd.Next(0, 3));
                    }

                    Point bloodPos;

                    TransformGroup transformGroup = new TransformGroup();
                    {
                        ScaleTransform scaleTransform = new ScaleTransform(
                            this.logic.Model.TileSizeWidth * Config.BloodMarkWidth / bloodBrush.MaxSize,
                            this.logic.Model.TileSizeHeight * Config.BloodMarkHeight / bloodBrush.MaxSize,
                            0.5,
                            0.5);

                        transformGroup.Children.Add(scaleTransform);

                        RotateTransform rotateTransform = new RotateTransform
                        {
                            CenterX = 0.5,
                            CenterY = 0.5,
                            Angle = bullet.Angle + 45,
                        };

                        transformGroup.Children.Add(rotateTransform);
                        bloodBrush.Brush.RelativeTransform = rotateTransform;

                        TranslateTransform translateTransform = new TranslateTransform();

                        int randomValue = Rnd.Next(4, 8);
                        for (int i = 0; i < randomValue; i++)
                        {
                            bullet.ChangeCoordinatesBySpeed();
                        }

                        bloodPos = this.logic.PositionOfEntityOnScreen(bullet);
                        translateTransform.X = bloodPos.X;
                        translateTransform.Y = bloodPos.Y;

                        transformGroup.Children.Add(translateTransform);
                    }

                    Geometry geometry = new RectangleGeometry(new Rect(0, 0, bloodBrush.MaxSize, bloodBrush.MaxSize), 0, 0, transformGroup);

                    this.cachedBullets.Add(new BulletDrawing(bloodPos, new GeometryDrawing(bloodBrush.Brush.Clone(), null, geometry), 255));
                }
            }

            drawingGroup.Children.Add(this.GetCachedBullets());

            var bullets = this.logic.Model.Bullets;

            // var bullets = new List<Bullet>(this.logic.Model.Bullets);
            if (bullets.Count > 0)
            {
                BrushClass bulletBrush = this.brushManager.BulletBrush();

                foreach (Bullet bullet in bullets)
                {
                    TransformGroup transformGroup = new TransformGroup();
                    {
                        ScaleTransform scaleTransform = new ScaleTransform(
                           this.logic.Model.TileSizeWidth * Config.BulletWidth / bulletBrush.MaxSize,
                           this.logic.Model.TileSizeHeight * Config.BulletHeight / bulletBrush.MaxSize,
                           0.5,
                           0.5);

                        transformGroup.Children.Add(scaleTransform);

                        RotateTransform rotateTransform = new RotateTransform
                        {
                            CenterX = 0.5,
                            CenterY = 0.5,
                            Angle = bullet.Angle,
                        };

                        transformGroup.Children.Add(rotateTransform);
                        bulletBrush.Brush.RelativeTransform = rotateTransform;

                        TranslateTransform translateTransform = new TranslateTransform();
                        Point bulletPos = this.logic.PositionOfEntityOnScreen(bullet);
                        translateTransform.X = bulletPos.X;
                        translateTransform.Y = bulletPos.Y;

                        transformGroup.Children.Add(translateTransform);
                    }

                    Geometry geometry = new RectangleGeometry(new Rect(0, 0, bulletBrush.MaxSize, bulletBrush.MaxSize), 0, 0, transformGroup);

                    drawingGroup.Children.Add(new GeometryDrawing(bulletBrush.Brush.Clone(), null, geometry));
                }

                bullets.RemoveAll(wallBullets.Contains);
                bullets.RemoveAll(entityBullets.Contains);
            }

            return drawingGroup;
        }

        private Drawing GetCachedBullets()
        {
            DrawingGroup drawingGroup = new DrawingGroup();
            for (int i = 0; i < this.cachedBullets.Count; i++)
            {
                if (this.cachedBullets[i].Opacity == 0)
                {
                    this.cachedBullets.RemoveAt(i);
                    continue;
                }

                var drawing = this.cachedBullets[i].Drawing;
                drawing.Brush.Opacity = Math.Min((byte)100, this.cachedBullets[i].Opacity) / 100.0;
                this.cachedBullets[i].Opacity--;
                drawingGroup.Children.Add(drawing);
            }

            return drawingGroup;
        }

        private void CenterDrawingForPlayer(DrawingGroup drawingGroup)
        {
            TranslateTransform translateTransform = new TranslateTransform();
            Point playerPos;
            IEntity entity = null;
            if (this.logic.Model.Player.Health > 0)
            {
                entity = this.logic.Model.Player;
                playerPos = this.logic.PositionOfEntityOnScreen(this.logic.Model.Player);
            }
            else
            {
                foreach (var item in this.logic.Model.Bots)
                {
                    if (item.Health > 0)
                    {
                        entity = item;
                        playerPos = this.logic.PositionOfEntityOnScreen(item);
                        break;
                    }
                }
            }

            this.lastPoint = playerPos;

            drawingGroup.Children.Add(this.GetHudInfo(entity));

            Point center = this.CenterOfScreen();
            Point transformedPos = new Point(center.X - playerPos.X, center.Y - playerPos.Y);
            translateTransform.X = transformedPos.X;
            translateTransform.Y = transformedPos.Y;

            drawingGroup.Transform = translateTransform;
        }

        private Drawing GetEntities()
        {
            DrawingGroup drawingGroup = new DrawingGroup();

            for (int i = 0; i < this.cachedEntities.Length; i++)
            {
                var cachedEntity = this.cachedEntities[i];
                var entity = i == this.cachedEntities.Length - 1 ? this.logic.Model.Player : this.logic.Model.Bots[i];

                if (entity.Health == 0)
                {
                    continue;
                }

                if (cachedEntity == null)
                {
                    cachedEntity = new CachedData();
                }

                if (cachedEntity.Drawing == null ||
                    cachedEntity.X != entity.X ||
                    cachedEntity.Y != entity.Y ||
                    cachedEntity.Angle != entity.Angle)
                {
                    cachedEntity.Drawing = this.GetEntity(entity);
                    cachedEntity.X = entity.X;
                    cachedEntity.Y = entity.Y;
                    cachedEntity.Angle = entity.Angle;
                }

                drawingGroup.Children.Add(cachedEntity.Drawing.Clone());
            }

            return drawingGroup;
        }

        private Drawing GetEntity(Entity entity)
        {
            BrushClass entityBrush = this.brushManager.EntityBrush(new BrushEntityData()
            {
                Model = entity.Model,
                WeaponModel = entity.HasBombInHands ? WeaponType.Bomb : entity.Weapon.WeaponType,
                ViewboxType = entity.HasBombInHands ? ViewboxType.Reload : ViewboxType.Pistol,
                HasBomb = entity.HasBomb,
            });

            TransformGroup transformGroup = new TransformGroup();
            {
                ScaleTransform scaleTransform = new ScaleTransform(
                    this.logic.Model.TileSizeWidth / entityBrush.MaxSize,
                    this.logic.Model.TileSizeHeight / entityBrush.MaxSize,
                    0.5,
                    0.5);

                transformGroup.Children.Add(scaleTransform);

                TranslateTransform translateTransform = new TranslateTransform();
                Point entityPos = this.logic.PositionOfEntityOnScreen(entity);
                translateTransform.X = entityPos.X;
                translateTransform.Y = entityPos.Y;

                transformGroup.Children.Add(translateTransform);
            }

            Geometry geometry = new RectangleGeometry(new Rect(0, 0, entityBrush.MaxSize, entityBrush.MaxSize), 0, 0, transformGroup);

            RotateTransform rotateTransform = new RotateTransform
            {
                CenterX = 0.5,
                CenterY = 0.5,
                Angle = entity.Angle,
            };
            entityBrush.Brush.RelativeTransform = rotateTransform;

            return new GeometryDrawing(entityBrush.Brush, null, geometry);
        }

        private DrawingGroup GenerateMap()
        {
            if (this.cachedMap == null)
            {
                DrawingGroup drawingGroup = new DrawingGroup();

                Point center = this.CenterOfScreen();
                Geometry backgroundGeometry = new RectangleGeometry(new Rect(
                    -center.X,
                    -center.Y,
                    3 * this.logic.Model.GameWidth,
                    (5 * this.logic.Model.GameHeight) + center.Y));
                GeometryDrawing background = new GeometryDrawing(Brushes.Black, null, backgroundGeometry);

                drawingGroup.Children.Add(background);

                GeometryGroup roadGeometry = new GeometryGroup();
                GeometryGroup bombsiteGeometry = new GeometryGroup();
                GeometryGroup wallGeometry = new GeometryGroup();

                for (int x = 0; x < this.logic.Model.Map.GetLength(0); x++)
                {
                    for (int y = 0; y < this.logic.Model.Map.GetLength(1); y++)
                    {
                        Geometry rect = new RectangleGeometry(new Rect(
                                            x * this.logic.Model.TileSizeWidth,
                                            y * this.logic.Model.TileSizeHeight,
                                            this.logic.Model.TileSizeWidth,
                                            this.logic.Model.TileSizeHeight));

                        switch (this.logic.Model.Map[x, y])
                        {
                            case NodeType.Wall:
                                wallGeometry.Children.Add(rect);
                                break;
                            case NodeType.Road:
                                roadGeometry.Children.Add(rect);
                                break;
                            case NodeType.BombSiteA:
                                roadGeometry.Children.Add(rect);
                                bombsiteGeometry.Children.Add(rect);
                                break;
                            case NodeType.BombSiteB:
                                break;
                            default:
                                break;
                        }
                    }
                }

                GeometryDrawing road = new GeometryDrawing(this.brushManager.RoadBrush().Brush, null, roadGeometry);
                drawingGroup.Children.Add(road);
                GeometryDrawing bombsite = new GeometryDrawing(this.brushManager.BombsiteBrush().Brush, null, bombsiteGeometry);
                drawingGroup.Children.Add(bombsite);
                GeometryDrawing walls = new GeometryDrawing(this.brushManager.WallBrush().Brush, null, wallGeometry);
                drawingGroup.Children.Add(walls);

                this.cachedMap = drawingGroup;
            }

            return this.cachedMap.Clone();
        }
    }
}
