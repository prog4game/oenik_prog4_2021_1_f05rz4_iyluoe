﻿// <copyright file="BrushManager.cs" company="Klavaro">
// Copyright (c) Klavaro. All rights reserved.
// </copyright>

namespace Wpf2DShooter.GameRendererNS
{
    using System;
    using System.Collections.Generic;
    using System.Drawing;
    using System.Linq;
    using System.Reflection;
    using System.Runtime.InteropServices;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Interop;
    using System.Windows.Media;
    using System.Windows.Media.Imaging;
    using Wpf2DShooter.GameLogicNS;
    using Wpf2DShooter.GameModelNS;

    /// <summary>
    /// Manages the brushes for everything which get's drawn in the game.
    /// </summary>
    internal class BrushManager
    {
        private IGameLogic logic;

        private BrushClass cachedBullet;
        private BrushClass cachedBulletHole;
        private BrushClass cachedBomb;
        private BrushClass cachedWall;
        private BrushClass cachedRoad;
        private BrushClass cachedBombsite;
        private Dictionary<ushort, BrushClass> entityBrushes;
        private Dictionary<byte, BrushClass> bloodBrushes;

        /// <summary>
        /// Initializes a new instance of the <see cref="BrushManager"/> class.
        /// </summary>
        /// <param name="logic">An <see cref="IGameLogic"/> implementation to work with.</param>
        public BrushManager(IGameLogic logic)
        {
            this.logic = logic;
            this.entityBrushes = new Dictionary<ushort, BrushClass>();
            this.bloodBrushes = new Dictionary<byte, BrushClass>();
        }

        /// <summary>
        /// Builds and caches an Entity brush for the game.
        /// </summary>
        /// <param name="data">A copy of a <see cref="BrushEntityData"/> struct.</param>
        /// <returns>The right Entity's brush.</returns>
        public BrushClass EntityBrush(BrushEntityData data)
        {
            ushort key = (ushort)(((ushort)data.Model * Enum.GetNames(data.Model.GetType()).Length) +
                ((ushort)data.WeaponModel * Enum.GetNames(data.WeaponModel.GetType()).Length) +
                ((ushort)data.ViewboxType * Enum.GetNames(data.ViewboxType.GetType()).Length) +
                Convert.ToUInt16(data.HasBomb));

            if (!this.entityBrushes.ContainsKey(key))
            {
                this.entityBrushes[key] =
                    this.LoadBrush($"Wpf2DShooter.GameRenderer.Images.PlayerModels.{Enum.GetName(data.Model)}.png|Wpf2DShooter.GameRenderer.Images.Weapons.{Enum.GetName(data.WeaponModel)}.png{(data.HasBomb ? "|Wpf2DShooter.GameRenderer.Images.Weapons.bomb_backpack.png" : string.Empty)}", false, data.ViewboxType);
            }

            return this.entityBrushes[key];
        }

        /// <summary>
        /// Builds and caches a Blood brush for the game.
        /// </summary>
        /// <param name="viewboxType">Describes if it's a puddle of blood or not.</param>
        /// <param name="index">The index of one of the blood image.</param>
        /// <returns>The right Blood brush.</returns>
        public BrushClass BloodBrush(ViewboxType viewboxType, byte index)
        {
            byte key = (byte)((byte)viewboxType * index);
            if (!this.bloodBrushes.ContainsKey(key))
            {
                this.bloodBrushes[key] =
                    this.LoadBrush("Wpf2DShooter.GameRenderer.Images.Misc.blood.png", false, viewboxType, index);
            }

            return this.bloodBrushes[key];
        }

        /// <summary>
        /// Builds and caches the Bullet brush for the game.
        /// </summary>
        /// <returns>Bullet brush.</returns>
        public BrushClass BulletBrush()
        {
            if (this.cachedBullet == null)
            {
                this.cachedBullet = this.LoadBrush("Wpf2DShooter.GameRenderer.Images.Misc.shells.png", false);
            }

            return this.cachedBullet;
        }

        /// <summary>
        /// Builds and caches the BulletHole brush for the game.
        /// </summary>
        /// <returns>BulletHole brush.</returns>
        public BrushClass BulletHoleBrush()
        {
            if (this.cachedBulletHole == null)
            {
                this.cachedBulletHole = this.LoadBrush("Wpf2DShooter.GameRenderer.Images.Misc.bullethole.png", false);
            }

            return this.cachedBulletHole;
        }

        /// <summary>
        ///  Builds and caches the BombBrush brush for the game.
        /// </summary>
        /// <returns>Bomb brush.</returns>
        public BrushClass BombBrush()
        {
            if (this.cachedBomb == null)
            {
                this.cachedBomb = this.LoadBrush("Wpf2DShooter.GameRenderer.Images.Weapons.bomb_planted.png", false);
            }

            return this.cachedBomb;
        }

        /// <summary>
        /// Builds and caches the Road brush for the game.
        /// </summary>
        /// <returns>Road brush.</returns>
        public BrushClass RoadBrush()
        {
            if (this.cachedRoad == null)
            {
                this.cachedRoad = this.LoadBrush("Wpf2DShooter.GameRenderer.Images.Tiles.ground.png", true);
            }

            return this.cachedRoad;
        }

        /// <summary>
        /// Builds and caches the Bombsite brush for the game.
        /// </summary>
        /// <returns>Road brush.</returns>
        public BrushClass BombsiteBrush()
        {
            if (this.cachedBombsite == null)
            {
                this.cachedBombsite = this.LoadBrush("Wpf2DShooter.GameRenderer.Images.Tiles.bombsite_a.png", true);
            }

            return this.cachedBombsite;
        }

        /// <summary>
        /// Builds and caches the Wall brush for the game.
        /// </summary>
        /// <returns>Wall brush.</returns>
        public BrushClass WallBrush()
        {
            if (this.cachedWall == null)
            {
                this.cachedWall = this.LoadBrush("Wpf2DShooter.GameRenderer.Images.Tiles.wall.png", true);
            }

            return this.cachedWall;
        }

        [DllImport("gdi32.dll")]
        [DefaultDllImportSearchPaths(DllImportSearchPath.UserDirectories)]
        private static extern bool DeleteObject(IntPtr hObject);

        private static ImageBrush InitBitmapImage(string filePath, ViewboxType viewboxType = ViewboxType.Default)
        {
            string[] files = filePath.Split('|');
            if (files.Length >= 2)
            {
                Bitmap playerModel = new Bitmap(Assembly.GetExecutingAssembly().GetManifestResourceStream(files[0]));

                int width = playerModel.Width;
                int height = playerModel.Height;

                int x = 0;
                int y = 0;

                width /= 2;
                height /= 3;

                switch (viewboxType)
                {
                    case ViewboxType.Rifle:
                        y = height * 2;
                        break;
                    case ViewboxType.Pistol:
                        x = width;
                        y = height;
                        break;
                }

                Bitmap weapon = new Bitmap(Assembly.GetExecutingAssembly().GetManifestResourceStream(files[1]));

                Bitmap result = new Bitmap(Math.Min(playerModel.Width, weapon.Width), Math.Min(playerModel.Height, weapon.Height));

                for (int i = 0; i < result.Width; i++)
                {
                    for (int j = 0; j < result.Height; j++)
                    {
                        System.Drawing.Color color = weapon.GetPixel(i, j);

                        if (color.A != 0)
                        {
                            result.SetPixel(i, j, color);
                        }
                    }
                }

                for (int i = x; i < x + width; i++)
                {
                    for (int j = y; j < y + height; j++)
                    {
                        System.Drawing.Color color = playerModel.GetPixel(i, j);

                        if (color.A != 0)
                        {
                            result.SetPixel(i - x, j - y, color);
                        }
                    }
                }

                if (files.Length == 3)
                {
                    using Bitmap bomb = new Bitmap(Assembly.GetExecutingAssembly().GetManifestResourceStream(files[2]));
                    for (int i = 0; i < bomb.Width; i++)
                    {
                        for (int j = 0; j < bomb.Height; j++)
                        {
                            System.Drawing.Color color = bomb.GetPixel(i, j);

                            if (color.A != 0)
                            {
                                result.SetPixel(i, j, color);
                            }
                        }
                    }
                }

                IntPtr hBitmap = result.GetHbitmap();
                BitmapSource bitmapSource;

                try
                {
                    bitmapSource = Imaging.CreateBitmapSourceFromHBitmap(
                                 hBitmap,
                                 IntPtr.Zero,
                                 Int32Rect.Empty,
                                 BitmapSizeOptions.FromEmptyOptions());
                }
                finally
                {
                    playerModel.Dispose();
                    weapon.Dispose();
                    result.Dispose();
                    DeleteObject(hBitmap);
                }

                ImageBrush brush = new ImageBrush();
                brush.ImageSource = bitmapSource;

                return brush;
            }

            BitmapImage bmp = new BitmapImage();
            bmp.BeginInit();
            bmp.StreamSource = Assembly.GetExecutingAssembly().GetManifestResourceStream(filePath);
            bmp.EndInit();

            return new ImageBrush(bmp);
        }

        private BrushClass LoadBrush(string filePath, bool isTiled, ViewboxType viewboxType = ViewboxType.Default, byte index = 0)
        {
            ImageBrush imageBrush = InitBitmapImage(filePath, viewboxType);

            imageBrush.AlignmentX = AlignmentX.Center;
            imageBrush.AlignmentY = AlignmentY.Center;

            if (isTiled)
            {
                imageBrush.TileMode = TileMode.Tile;
                imageBrush.Viewport = new Rect(0, 0, this.logic.Model.TileSizeWidth, this.logic.Model.TileSizeHeight);
                imageBrush.ViewportUnits = BrushMappingMode.Absolute;
            }

            double width = imageBrush.ImageSource.Width;
            double height = imageBrush.ImageSource.Height;

            if (viewboxType == ViewboxType.ShotBlood || viewboxType == ViewboxType.PuddleBlood)
            {
                double x = 0;
                double y = 0;

                width /= 5;
                x = width * index;

                imageBrush.Viewbox = new Rect(x, y, width, height);
                imageBrush.ViewboxUnits = BrushMappingMode.Absolute;
            }

            double maxSize = Math.Sqrt(Math.Pow(width, 2) + Math.Pow(height, 2));

            return new BrushClass() { Brush = imageBrush, MaxSize = maxSize };
        }
    }
}
