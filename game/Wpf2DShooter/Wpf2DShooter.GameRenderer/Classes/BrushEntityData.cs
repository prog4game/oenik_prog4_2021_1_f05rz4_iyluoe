﻿// <copyright file="BrushEntityData.cs" company="Klavaro">
// Copyright (c) Klavaro. All rights reserved.
// </copyright>

namespace Wpf2DShooter.GameRendererNS
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows.Media;
    using Wpf2DShooter.GameModelNS;
    using Wpf2DShooter.GameRendererNS;

    /// <summary>
    /// Helper class for Entity <see cref="Brush"/>es by discribing input data about an Entity.
    /// </summary>
    internal struct BrushEntityData
    {
        /// <summary>
        /// The identificator of the model of the entity.
        /// </summary>
        public ModelType Model;

        /// <summary>
        /// The identificator of the model of the weapon.
        /// </summary>
        public WeaponType WeaponModel;

        /// <summary>
        /// The identificator of the model's viewbox of the entity.
        /// </summary>
        public ViewboxType ViewboxType;

        /// <summary>
        /// Tells if the entity has a bomb or not.
        /// </summary>
        public bool HasBomb;
    }
}
