﻿// <copyright file="BrushClass.cs" company="Klavaro">
// Copyright (c) Klavaro. All rights reserved.
// </copyright>

namespace Wpf2DShooter.GameRendererNS
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows.Media;

    /// <summary>
    /// Helper class for <see cref="Brush"/>es by also providing max size of the image.
    /// </summary>
    internal class BrushClass
    {
        /// <summary>
        /// Gets or Sets: The <see cref="Brush"/>.
        /// </summary>
        public Brush Brush { get; set; }

        /// <summary>
        /// Gets or Sets: The max size of the image.
        /// </summary>
        public double MaxSize { get; set; }
    }
}
