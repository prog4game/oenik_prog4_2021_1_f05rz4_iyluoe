﻿// <copyright file="SoundEffectType.cs" company="Klavaro">
// Copyright (c) Klavaro. All rights reserved.
// </copyright>

namespace Wpf2DShooter.GameLogicNS
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Describes the different type of sound effects in the game.
    /// </summary>
    public enum SoundEffectType
    {
        /// <summary>
        /// Default enum.
        /// </summary>
        None,

        /// <summary>
        /// Sound effect for shooting the USP;
        /// </summary>
        USPShoot,

        /// <summary>
        /// Sound effect for reloading the USP;
        /// </summary>
        USPReload,

        /// <summary>
        /// Sound effect for shooting the Glock;
        /// </summary>
        GlockShoot,

        /// <summary>
        /// Sound effect for reloading the USP;
        /// </summary>
        GlockReload,

        /// <summary>
        /// Sound effect for arming the bomb.
        /// </summary>
        BombArming,

        /// <summary>
        /// Sound effect for defusing the bomb.
        /// </summary>
        BombDefusing,

        /// <summary>
        /// Sound effect for the planted bomb.
        /// </summary>
        BombPlanted,

        /// <summary>
        /// Sound effect for the defused bomb.
        /// </summary>
        BombDefused,

        /// <summary>
        /// Sound effect for the exploding bomb.
        /// </summary>
        BombExploded,

        /// <summary>
        /// First sound effect for getting hit by a bullet.
        /// </summary>
        BulletHit1,

        /// <summary>
        /// Second sound effect for getting hit by a bullet.
        /// </summary>
        BulletHit2,

        /// <summary>
        /// First sound effect for getting killed by a bullet.
        /// </summary>
        Death1,

        /// <summary>
        /// Second sound effect for getting killed by a bullet.
        /// </summary>
        Death2,

        /// <summary>
        /// Third sound effect for getting killed by a bullet.
        /// </summary>
        Death3,

        /// <summary>
        /// Sound effect if the Counter Terrorists won;
        /// </summary>
        CounterTerroristsWon,

        /// <summary>
        /// Sound effect if the Terrorists won;
        /// </summary>
        TerroristsWon,
    }
}
