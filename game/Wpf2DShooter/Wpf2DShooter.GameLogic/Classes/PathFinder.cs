﻿// <copyright file="PathFinder.cs" company="Klavaro">
// Copyright (c) Klavaro. All rights reserved.
// </copyright>

namespace Wpf2DShooter.GameLogicNS
{
    using System;
    using System.Collections.Generic;
    using System.Drawing;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Wpf2DShooter.GameModelNS;

    /// <summary>
    /// Used by <see cref="GameLogic"/> for the bots movements.
    /// </summary>
    internal class PathFinder
    {
        private const int EmptyValue = 0;
        private const int FinishValue = 255;
        private const int StartValue = -1;
        private const int ObstacleValue = -3;

        private int[,] map;

        private NodeType[,] nodeMap;
        private Point start;
        private Point end;

        /// <summary>
        /// Initializes a new instance of the <see cref="PathFinder"/> class.
        /// </summary>
        /// <param name="nodeMap">The map of the game.</param>
        /// <param name="start">The starting point of the route.</param>
        /// <param name="end">The ending point of the route.</param>
        public PathFinder(NodeType[,] nodeMap, Point start, Point end)
        {
            this.nodeMap = nodeMap;
            this.start = start;
            this.end = end;
        }

        /// <summary>
        /// Gets or Sets a value indicating whether : The path has been completed or not.
        /// </summary>
        public bool PathComplete { get; set; }

        /// <summary>
        /// Gets : The End point.
        /// </summary>
        public Point End { get => this.end; }

        /// <summary>
        /// Gets or Sets : The current X coordinate of the entity.
        /// </summary>
        public int X { get; set; }

        /// <summary>
        /// Gets or Sets : The current Y coordinate of the entity.
        /// </summary>
        public int Y { get; set; }

        /// <summary>
        /// Builds the map of navigation.
        /// </summary>
        public void ConstructMap()
        {
            int[,] newMap = new int[this.nodeMap.GetLength(0), this.nodeMap.GetLength(1)];

            for (int i = 0; i < newMap.GetLength(0); i++)
            {
                for (int j = 0; j < newMap.GetLength(1); j++)
                {
                    if (this.nodeMap[i, j] == NodeType.Wall || this.nodeMap[i, j] == NodeType.None)
                    {
                        newMap[i, j] = ObstacleValue;
                    }
                }
            }

            newMap[this.start.X, this.start.Y] = StartValue;
            newMap[this.end.X, this.end.Y] = FinishValue;

            this.X = this.start.X;
            this.Y = this.start.Y;

            this.map = newMap;

            this.Wavefront();
        }

        /*private void CopyFrom(ref int[,] destination, ref int[,] original)
        {
            for (int i = 0; i < original.GetLength(0); i++)
            {
                for (int j = 0; j < original.GetLength(1); j++)
                {
                    destination[i, j] = original[i, j];
                }
            }
        }*/

        /// <summary>
        /// Searches for the best direction.
        /// </summary>
        /// <returns>The best direction.</returns>
        public Point GetNextStep()
        {
            Point returnValue = new Point(this.X, this.Y);
            if (this.map != null)
            {
                double localMin = int.MaxValue;
                for (int i = -1; i <= 1; i++)
                {
                    for (int j = -1; j <= 1; j++)
                    {
                        double cellValue = this.map[this.X + i, this.Y + j];

                        if (i == 0 && j == 0)
                        {
                            if (cellValue == FinishValue)
                            {
                                returnValue = new Point(this.X + i, this.Y + j);
                                return returnValue;
                            }

                            continue;
                        }

                        if (Math.Abs(i) == 1 && Math.Abs(j) == 1)
                        {
                            if (this.map[this.X + i, this.Y] == ObstacleValue || this.map[this.X, this.Y + j] == ObstacleValue)
                            {
                                continue;
                            }
                        }

                        if (cellValue == FinishValue)
                        {
                            // this.PathComplete = true;
                            returnValue = new Point(this.X + i, this.Y + j);
                            break;
                        }
                        else if (cellValue > 0 && cellValue < localMin)
                        {
                            localMin = cellValue;
                            returnValue = new Point(this.X + i, this.Y + j);
                        }
                    }
                }
            }

            return returnValue;
        }

        private static double Distance(double firstX, double firstY, double secondX, double secondY)
        {
            return Math.Sqrt(Math.Pow(firstX - secondX, 2) + Math.Pow(firstY - secondY, 2));
        }

        /*private void Shitfront()
        {
            int newWavefrontCells = 1;
            while (newWavefrontCells > 0)
            {
                newWavefrontCells = 0;

                for (int i = 1; i < this.map.GetLength(0) - 1; i++)
                {
                    for (int j = 1; j < this.map.GetLength(1) - 1; j++)
                    {
                        if (this.map[i, j] == EmptyValue)
                        {
                            double localMin = int.MaxValue;
                            int min_k = -1;
                            int min_l = -1;

                            for (int k = -1; k <= 1; k++)
                            {
                                for (int l = -1; l <= 1; l++)
                                {

                                    double cellValue = this.map[i + k, j + l];
                                    if (cellValue > EmptyValue)
                                    {
                                        if (cellValue < localMin)
                                        {
                                            localMin = cellValue;
                                            min_k = k;
                                            min_l = l;
                                        }
                                    }
                                    else if (cellValue == FinishValue)
                                    {
                                        localMin = cellValue;
                                        min_k = k;
                                        min_l = l;
                                    }
                                }
                            }

                            if (localMin < int.MaxValue)
                            {
                                if (i == 31 && j == 14)
                                {
                                    ;
                                }
                                if (Math.Abs(min_k) == 1 && Math.Abs(min_l) == 1)
                                {
                                    this.map[i, j] = localMin + 3 + Distance(i + min_k, j + min_l, this.End.X, this.End.Y);
                                }
                                else
                                {
                                    this.map[i, j] = localMin + 2 + Distance(i + min_k, j + min_l, this.End.X, this.End.Y);
                                }

                                newWavefrontCells++;
                            }
                        }
                    }
                }
            }
        }*/

        private void Wavefront(int x, int y)
        {
            if (x < 0 || x >= this.map.GetLength(1) || y < 0 || y >= this.map.GetLength(0))
            {
                return;
            }

            List<Point> pointsToCheck = new List<Point>();

            for (int ty = -1; ty <= 1; ty++)
            {
                for (int tx = -1; tx <= 1; tx++)
                {
                    int posX = x + tx;
                    int posY = y + ty;

                    if (posX < 0 || posX >= this.map.GetLength(1) || posY < 0 || posY >= this.map.GetLength(0))
                    {
                        continue;
                    }

                    if (this.map[posY, posX] == ObstacleValue || this.map[posY, posX] == FinishValue)
                    {
                        continue;
                    }

                    int nextValue = ty != 0 && tx != 0 ? 3 : 2;

                    if (this.map[posY, posX] == EmptyValue || this.map[posY, posX] > this.map[y, x] + nextValue)
                    {
                        this.map[posY, posX] = (this.map[y, x] == -1 ? 0 : this.map[y, x]) + nextValue;
                        pointsToCheck.Add(new Point(posX, posY));
                    }
                }
            }

            pointsToCheck.ForEach((x) => { this.Wavefront(x.X, x.Y); });
        }

        private void Wavefront()
        {
            this.Wavefront(this.End.Y, this.End.X);
        }
    }
}
