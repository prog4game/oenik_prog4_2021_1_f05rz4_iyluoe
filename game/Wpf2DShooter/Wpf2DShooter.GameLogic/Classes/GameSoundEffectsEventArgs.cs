﻿// <copyright file="GameSoundEffectsEventArgs.cs" company="Klavaro">
// Copyright (c) Klavaro. All rights reserved.
// </copyright>

namespace Wpf2DShooter.GameLogicNS
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Describes the data required to play a sound effect.
    /// </summary>
    public class GameSoundEffectsEventArgs : EventArgs
    {
        /// <summary>
        /// Gets or Sets : The Sound effect.
        /// </summary>
        public SoundEffectType SoundEffect { get; set; }

        /// <summary>
        /// Gets or Sets a value indicating whether : The sound should be stopped or played.
        /// </summary>
        public bool StopSound { get; set; }
    }
}
