﻿// <copyright file="Config.cs" company="Klavaro">
// Copyright (c) Klavaro. All rights reserved.
// </copyright>

namespace Wpf2DShooter.GameLogicNS
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Defines constant values of the game.
    /// </summary>
    public static class Config
    {
        /// <summary>
        /// Defines the number of counter terrorists in the game.
        /// </summary>
        public const byte NumberOfCounterTerrorists = 5;

        /// <summary>
        /// Defines the number of terrorists in the game.
        /// </summary>
        public const byte NumberOfTerrorists = 5;

        /// <summary>
        /// Defines the maximum health of the entities in the game.
        /// </summary>
        public const short MaxHealth = 100;

        /// <summary>
        /// Defines the size of a step an entity makes.
        /// </summary>
        public const double StepSize = 0.1;

        /// <summary>
        /// Defines the size of a bombsite.
        /// </summary>
        public const double BombSiteSize = 1.5;

        /// <summary>
        /// Defines the size of a tile in the game.
        /// </summary>
        public const double TileSizeWidth = 75; // 50

        /// <summary>
        /// Defines the size of a tile in the game.
        /// </summary>
        public const double TileSizeHeight = 75; // 50

        /// <summary>
        /// Defines the size of a bullet in the game.
        /// </summary>
        public const double BulletWidth = 1.0 / 8.0;

        /// <summary>
        /// Defines the size of a bullet in the game.
        /// </summary>
        public const double BulletHeight = 1.0 / 4.0;

        /// <summary>
        /// Defines the size of a bullet in the game.
        /// </summary>
        public const double BulletHoleWidth = 1.0 / 10.0;

        /// <summary>
        /// Defines the size of a bullet in the game.
        /// </summary>
        public const double BulletHoleHeight = 1.0 / 10.0;

        /// <summary>
        /// Defines the size of a blood mark in the game.
        /// </summary>
        public const double BloodMarkWidth = 2.0 / 3.0;

        /// <summary>
        /// Defines the size of a blood mark in the game.
        /// </summary>
        public const double BloodMarkHeight = 2.0 / 3.0;

        /// <summary>
        /// Defines the time needed for arming a bomb.
        /// </summary>
        public const byte BombArmingTime = 50;

        /// <summary>
        /// Defines the time of every rounds beggining when the entities can't move.
        /// </summary>
        public const byte FreezeTime = 5;

        /// <summary>
        /// Defines the time of every round.
        /// </summary>
        public const byte RoundTime = 60;

        /// <summary>
        /// Defines the time needed for a bomb to explode.
        /// </summary>
        public const byte BombTime = 30;
    }
}
