﻿// <copyright file="GameLogic.cs" company="Klavaro">
// Copyright (c) Klavaro. All rights reserved.
// </copyright>

namespace Wpf2DShooter.GameLogicNS
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Reflection;
    using System.Text;
    using System.Threading.Tasks;
    using System.Timers;
    using System.Windows;
    using Wpf2DShooter.GameLogicNS;
    using Wpf2DShooter.GameModelNS;

    /// <summary>
    /// Provides an implementation of the <see cref="IGameLogic"/> interface.
    /// </summary>
    public class GameLogic : IGameLogic
    {
        private static readonly Random Rnd = new Random();
        private IGameModel model;
        private Point bombsite;
        private List<PathFinder> pathFinders;
        private List<Point> counterTerroristSpawnPoints;
        private List<Point> terroristSpawnPoints;
        private List<System.Drawing.Point> campPoints;
        private Timer freezeTimer;
        private Timer roundTimer;
        private Timer bombTimer;

        private bool isDisposed;

        /// <summary>
        /// Initializes a new instance of the <see cref="GameLogic"/> class.
        /// </summary>
        /// <param name="model">An <see cref="IGameModel"/> implementation to work with.</param>
        public GameLogic(IGameModel model)
        {
            this.Model = model;
            this.InitGame(Lvl1);
        }

        private event EventHandler<GameSoundEffectsEventArgs> SoundEvent;

        /// <inheritdoc/>
        public IGameModel Model { get => this.model; set => this.model = value; }

        private static string Lvl1
        {
            get { return "Wpf2DShooter.GameLogic.Resources.lvl1.map"; }
        }

        /// <inheritdoc/>
        public void AddSoundEventHandler(EventHandler<GameSoundEffectsEventArgs> eventHandler)
        {
            this.SoundEvent += eventHandler;
        }

        /// <inheritdoc/>
        public void InitGame(string mapSrc)
        {
            Stream stream = Assembly.GetExecutingAssembly().GetManifestResourceStream(mapSrc);
            if (stream == null)
            {
                return;
            }

            string[] lines;

            using (StreamReader reader = new StreamReader(stream))
            {
                lines = reader.ReadToEnd().Split('\n');
            }

            ushort width = ushort.Parse(lines[0], null);
            ushort height = ushort.Parse(lines[1], null);

            this.Model.Map = new NodeType[width, height];
            this.Model.TileSizeWidth = Config.TileSizeWidth;
            this.Model.TileSizeHeight = Config.TileSizeHeight;

            List<Point> counterTerroristSpawnPoints = new List<Point>(Config.NumberOfCounterTerrorists);
            List<Point> terroristSpawnPoints = new List<Point>(Config.NumberOfTerrorists);
            List<System.Drawing.Point> campPoints = new List<System.Drawing.Point>();

            for (ushort x = 0; x < width; x++)
            {
                for (ushort y = 0; y < height; y++)
                {
                    if (lines.Length - 2 <= y || lines[y + 2].Length <= x)
                    {
                        continue;
                    }

                    char current = lines[y + 2][x];
                    if (current == 'W')
                    {
                        this.Model.Map[x, y] = NodeType.Wall;
                    }
                    else if (current == 'R')
                    {
                        this.Model.Map[x, y] = NodeType.Road;
                    }
                    else if (current == 'C')
                    {
                        this.Model.Map[x, y] = NodeType.Road;
                        counterTerroristSpawnPoints.Add(new Point(x, y));
                    }
                    else if (current == 'T')
                    {
                        this.Model.Map[x, y] = NodeType.Road;
                        terroristSpawnPoints.Add(new Point(x, y));
                    }
                    else if (current == 'S')
                    {
                        this.Model.Map[x, y] = NodeType.Road;
                        campPoints.Add(new System.Drawing.Point(x, y));
                    }
                    else if (current == 'A')
                    {
                        this.Model.Map[x, y] = NodeType.BombSiteA;
                    }
                }
            }

            this.Model.Bots = new List<Entity>();
            this.model.Bomb = new Bomb();
            this.counterTerroristSpawnPoints = new List<Point>(counterTerroristSpawnPoints);
            this.terroristSpawnPoints = new List<Point>(terroristSpawnPoints);

            if (this.model.ChosenTeam == TeamType.CounterTerrorist)
            {
                this.InitEntities(counterTerroristSpawnPoints, TeamType.CounterTerrorist);
                this.InitEntities(terroristSpawnPoints, TeamType.Terrorist);
            }
            else
            {
                this.InitEntities(terroristSpawnPoints, TeamType.Terrorist);
                this.InitEntities(counterTerroristSpawnPoints, TeamType.CounterTerrorist);
            }

            this.campPoints = new List<System.Drawing.Point>(campPoints);
            this.freezeTimer = new Timer(1000);
            this.freezeTimer.Elapsed += this.FreezeTimer_Elapsed;
            this.roundTimer = new Timer(1000);
            this.roundTimer.Elapsed += this.RoundTimer_Elapsed;
            this.bombTimer = new Timer(1000);
            this.bombTimer.Elapsed += this.BombTimer_Elapsed;
            this.model.FreezeTime = true;
            this.FreezeTimeStuff(campPoints);
            this.FreezeTimeWait(true).Start();
        }

        /// <inheritdoc/>
        public bool SetPlayerVelocity(Direction direction)
        {
            if (this.model.FreezeTime)
            {
                return false;
            }

            double oldDX = this.model.Player.DX;
            double oldDY = this.model.Player.DY;
            if (this.model.Player.DX != 0)
            {
                this.model.Player.DX = ClampMagnitude(this.model.Player.DX);
                this.UpdateXAxis();
            }

            if (this.model.Player.DY != 0)
            {
                this.model.Player.DY = ClampMagnitude(this.model.Player.DY);
                this.UpdateYAxis();
            }

            double x = this.model.Player.X;
            double y = this.model.Player.Y;
            if (this.IsEntityMovable(ref x, ref y, direction))
            {
                this.model.Player.DX = x - this.model.Player.X;
                this.model.Player.DY = y - this.model.Player.Y;

                if (oldDX != 0)
                {
                    this.UpdateXAxis();
                }

                if (oldDY != 0)
                {
                    this.UpdateYAxis();
                }

                return true;
            }

            return false;
        }

        /// <inheritdoc/>
        public void UpdateEntityPos(IMovable entity)
        {
            if (entity == null)
            {
                throw new ArgumentNullException(nameof(entity), "Entity shouldn't be null!");
            }

            entity.X += Math.Round(entity.DX, 8);
            entity.Y += Math.Round(entity.DY, 8);

            entity.DX = 0;
            entity.DY = 0;
        }

        /// <inheritdoc/>
        public Point PositionOfEntityOnScreen(IMovable entity)
        {
            return entity != null ? new Point(entity.X * this.model.TileSizeWidth, entity.Y * this.model.TileSizeHeight)
               : throw new ArgumentNullException(nameof(entity), "Entity shouldn't be null!");
        }

        /// <inheritdoc/>
        public void HandleAttack(IEntity attacker)
        {
            if (attacker == null)
            {
                throw new ArgumentNullException(nameof(attacker), "Attacker shouldn't be null!");
            }

            if (attacker.HasBombInHands)
            {
                this.PlantBomb(attacker);
            }
            else if (attacker.Team == TeamType.CounterTerrorist && this.AtBombsite(attacker))
            {
                this.DefuseBomb(attacker);
            }
            else
            {
                this.Shoot(attacker);
            }
        }

        /// <inheritdoc/>
        public bool SwitchToBomb(IEntity entity)
        {
            if (entity == null)
            {
                throw new ArgumentNullException(nameof(entity), "Entity shouldn't be null!");
            }

            if (!entity.HasBomb)
            {
                return false;
            }

            entity.HasBombInHands = !entity.HasBombInHands;

            return true;
        }

        /// <inheritdoc/>
        public void HandleBulletsAndReturnListOfBulletsThatHitSomething(out List<IBullet> wall, out List<IBullet> entities)
        {
            var wallList = new List<IBullet>();
            var entitiesList = new List<IBullet>();

            bool roundEnded = false;

            try
            {
                this.model.Bullets.ForEach(bullet =>
                {
                    bullet.ChangeCoordinatesBySpeed();

                    if (this.DoesObjectCollidesWithAWall(bullet.X, bullet.Y, true))
                    {
                        wallList.Add(bullet);
                    }

                    if (this.DoesObjectCollidesWithAnEntity(bullet, out IEntity hitEntity, bullet.Owner))
                    {
                        if (bullet.Owner.Team != hitEntity.Team)
                        {
                            hitEntity.Health -= (short)bullet.Damage;
                            if (hitEntity.Health <= 0)
                            {
                                hitEntity.Health = 0;
                                bullet.WasLethal = true;

                                if (hitEntity.HasBomb)
                                {
                                    this.model.Bomb.FinishedArming(false);
                                    if (this.model.Bomb.JustStoppedArming)
                                    {
                                        this.SoundEvent?.Invoke(null, new GameSoundEffectsEventArgs() { SoundEffect = SoundEffectType.BombArming, StopSound = true });
                                        this.model.Bomb.JustStoppedArming = false;
                                    }

                                    this.model.Bomb.IsOnGround = true;
                                    this.model.Bomb.X = hitEntity.X;
                                    this.model.Bomb.Y = hitEntity.Y;
                                }

                                if (!roundEnded)
                                {
                                    roundEnded = this.CheckIfGamesEnded();
                                    if (roundEnded)
                                    {
                                        this.roundTimer.Stop();
                                    }
                                }

                                this.SoundEvent?.Invoke(null, new GameSoundEffectsEventArgs() { SoundEffect = (SoundEffectType)((int)SoundEffectType.Death1 + Rnd.Next(0, 3)) });
                            }
                        }

                        this.SoundEvent?.Invoke(null, new GameSoundEffectsEventArgs() { SoundEffect = (SoundEffectType)((int)SoundEffectType.BulletHit1 + Rnd.Next(0, 2)) });

                        entitiesList.Add(bullet);
                    }
                });
            }
            catch (System.InvalidOperationException)
            {
            }

            wall = wallList;
            entities = entitiesList;
        }

        /// <inheritdoc/>
        public void HandleWeaponCoolDowns()
        {
            this.model.Player.Weapon.CanShoot();
            this.model.Bots.ForEach(bot => bot.Weapon.CanShoot());
        }

        /// <inheritdoc/>
        public void MoveBots()
        {
            if (!this.model.FreezeTime)
            {
                this.model.Bots.ForEach(bot => this.BotMovement(bot));
            }
        }

        /// <inheritdoc/>
        public void Dispose()
        {
            this.Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Dispose() calls Dispose(true).
        /// </summary>
        /// <param name="disposing">Local parameter.</param>
        protected virtual void Dispose(bool disposing)
        {
            if (this.isDisposed)
            {
                return;
            }

            if (disposing)
            {
                this.freezeTimer.Dispose();
                this.roundTimer.Dispose();
                this.bombTimer.Dispose();
            }

            this.isDisposed = true;
        }

        private static double FractionalPartOfNumber(double number)
        {
            if (number - Math.Truncate(number) < Config.StepSize)
            {
                return 0;
            }

            return 1;
        }

        private static double ClampMagnitude(double number)
        {
            double magnitude = Math.Sqrt(2 * Math.Pow(number, 2));
            return number * Config.StepSize / magnitude;
        }

        private static double Distance(IMovable first, IMovable second)
        {
            return Math.Pow(first.X - second.X, 2) + Math.Pow(first.Y - second.Y, 2);
        }

        private static double Distance(double firstX, double firstY, double secondX, double secondY)
        {
            return Math.Pow(firstX - secondX, 2) + Math.Pow(firstY - secondY, 2);
        }

        private void FreezeTimer_Elapsed(object sender, ElapsedEventArgs e)
        {
            if (this.model.RoundTime.TotalSeconds == 0)
            {
                this.freezeTimer.Stop();
                this.model.RoundTime = TimeSpan.FromSeconds(Config.RoundTime);
                this.roundTimer.Start();
            }

            this.model.RoundTime = this.model.RoundTime.Subtract(TimeSpan.FromSeconds(1));
        }

        private void RoundTimer_Elapsed(object sender, ElapsedEventArgs e)
        {
            if (this.model.RoundTime.TotalSeconds == 0)
            {
                this.roundTimer.Stop();
                this.SoundEvent?.Invoke(null, new GameSoundEffectsEventArgs() { SoundEffect = SoundEffectType.CounterTerroristsWon });
                this.model.NumberOfWonRoundsByCounterTerrorists++;
                this.NextRound();
            }

            this.model.RoundTime = this.model.RoundTime.Subtract(TimeSpan.FromSeconds(1));
        }

        private void BombTimer_Elapsed(object sender, ElapsedEventArgs e)
        {
            if (this.model.RoundTime.TotalSeconds == 0)
            {
                this.bombTimer.Stop();
                this.SoundEvent?.Invoke(null, new GameSoundEffectsEventArgs() { SoundEffect = SoundEffectType.BombExploded });
                this.model.NumberOfWonRoundsByTerrorists++;
                this.NextRound();
            }

            this.model.RoundTime = this.model.RoundTime.Subtract(TimeSpan.FromSeconds(1));
        }

        private Task FreezeTimeWait(bool unlock = false)
        {
            this.model.RoundTime = TimeSpan.FromSeconds(Config.FreezeTime);
            this.freezeTimer.Start();
            return new Task(() =>
            {
                System.Threading.Thread.Sleep(Config.FreezeTime * 1000);
                if (unlock)
                {
                    this.model.FreezeTime = false;
                }
            });
        }

        private void FreezeTimeStuff(List<System.Drawing.Point> campPoints)
        {
            this.pathFinders = this.model.Bots.Select(x => new PathFinder(
                this.model.Map,
                new System.Drawing.Point((int)x.X, (int)x.Y),
                this.GetPositionOnMap(campPoints, x.Team))).ToList();

            this.pathFinders.Select(t => new Task(
                        () => { t.ConstructMap(); },
                        TaskCreationOptions.LongRunning)).ToList().ForEach(t => t.Start());
        }

        private System.Drawing.Point GetPositionOnMap(List<System.Drawing.Point> campPoints, TeamType team)
        {
            if (team == TeamType.CounterTerrorist)
            {
                var index = Rnd.Next(0, campPoints.Count);
                var returnValue = campPoints[index];
                campPoints.RemoveAt(index);
                return returnValue;
            }
            else
            {
                if (this.bombsite == default)
                {
                    for (int x = 0; x < this.model.Map.GetLength(0); x++)
                    {
                        for (int y = 0; y < this.model.Map.GetLength(1); y++)
                        {
                            if (this.model.Map[x, y] == NodeType.BombSiteA)
                            {
                                this.bombsite = new Point(x + 0.5, y + 0.5);
                                break;
                            }
                        }
                    }
                }

                return new System.Drawing.Point(
                    Rnd.Next(-(int)Config.BombSiteSize, (int)Config.BombSiteSize) + (int)this.bombsite.X,
                    Rnd.Next(-(int)Config.BombSiteSize, (int)Config.BombSiteSize) + (int)this.bombsite.Y);
            }
        }

        private bool CheckIfGamesEnded()
        {
            if (this.model.Player.Team == TeamType.CounterTerrorist)
            {
                if (this.model.Bots.Where(x => x.Team == TeamType.CounterTerrorist).All(x => x.Health == 0) && this.model.Player.Health == 0)
                {
                    this.SoundEvent?.Invoke(null, new GameSoundEffectsEventArgs() { SoundEffect = SoundEffectType.TerroristsWon });
                    this.model.NumberOfWonRoundsByTerrorists++;
                    this.NextRound();
                    return true;
                }
                else if (this.model.Bots.Where(x => x.Team == TeamType.Terrorist).All(x => x.Health == 0) && !this.model.Bomb.IsPlanted)
                {
                    this.SoundEvent?.Invoke(null, new GameSoundEffectsEventArgs() { SoundEffect = SoundEffectType.CounterTerroristsWon });
                    this.model.NumberOfWonRoundsByCounterTerrorists++;
                    this.NextRound();
                    return true;
                }
            }
            else
            {
                if (this.model.Bots.Where(x => x.Team == TeamType.Terrorist).All(x => x.Health == 0) && this.model.Player.Health == 0 && !this.model.Bomb.IsPlanted)
                {
                    this.SoundEvent?.Invoke(null, new GameSoundEffectsEventArgs() { SoundEffect = SoundEffectType.CounterTerroristsWon });
                    this.model.NumberOfWonRoundsByCounterTerrorists++;
                    this.NextRound();
                    return true;
                }
                else if (this.model.Bots.Where(x => x.Team == TeamType.CounterTerrorist).All(x => x.Health == 0))
                {
                    this.SoundEvent?.Invoke(null, new GameSoundEffectsEventArgs() { SoundEffect = SoundEffectType.TerroristsWon });
                    this.model.NumberOfWonRoundsByTerrorists++;
                    this.NextRound();
                    return true;
                }
            }

            return false;
        }

        private void NextRound()
        {
            new Task(() =>
            {
                System.Threading.Thread.Sleep(3000);
                this.SoundEvent?.Invoke(null, new GameSoundEffectsEventArgs() { SoundEffect = SoundEffectType.BombPlanted, StopSound = true });
                this.model.FreezeTime = true;
                var freezeTask = this.FreezeTimeWait();
                freezeTask.Start();
                this.InitEntities(new List<Point>(this.terroristSpawnPoints), TeamType.Terrorist, false);
                this.InitEntities(new List<Point>(this.counterTerroristSpawnPoints), TeamType.CounterTerrorist, false);
                this.FreezeTimeStuff(new List<System.Drawing.Point>(this.campPoints));
                freezeTask.Wait();
                this.model.FreezeTime = false;
            }).Start();
        }

        private void BotMovement(Entity bot)
        {
            if (bot.Health > 0)
            {
                bot.HasBombInHands = false;

                for (int i = 0; i <= this.model.Bots.Count; i++)
                {
                    var entity = i == this.model.Bots.Count ? this.model.Player : this.model.Bots[i];

                    if (entity == bot)
                    {
                        continue;
                    }

                    if (entity.Team == bot.Team)
                    {
                        continue;
                    }

                    if (this.CanSeeAnotherEntity(bot, entity, out double aimAngle))
                    {
                        this.model.Bomb.FinishedDefusing(false);
                        if (this.model.Bomb.JustStoppedDefusing)
                        {
                            this.SoundEvent?.Invoke(null, new GameSoundEffectsEventArgs() { SoundEffect = SoundEffectType.BombDefusing, StopSound = true });
                            this.model.Bomb.JustStoppedDefusing = false;
                        }

                        bot.Angle = aimAngle;
                        this.Shoot(bot);
                        return;
                    }
                }

                var index = this.model.Bots.IndexOf(bot);

                if (bot.Team == TeamType.CounterTerrorist)
                {
                    if (this.model.Bomb.IsPlanted)
                    {
                        if (this.pathFinders[index].End.X != (int)this.bombsite.X || this.pathFinders[index].End.Y != (int)this.bombsite.Y)
                        {
                            this.pathFinders[index] = new PathFinder(
                                this.model.Map,
                                new System.Drawing.Point((int)bot.X, (int)bot.Y),
                                new System.Drawing.Point((int)this.bombsite.X, (int)this.bombsite.Y));
                            this.pathFinders[index].ConstructMap();
                            return;
                        }

                        if (this.pathFinders[index].PathComplete)
                        {
                            this.HandleAttack(bot);
                            return;
                        }
                    }
                }
                else
                {
                    var bombsite = new System.Drawing.Point((int)this.bombsite.X, (int)this.bombsite.Y);
                    if (!this.model.Bomb.IsPlanted)
                    {
                        if (bot.HasBomb)
                        {
                            if (this.pathFinders[index].End.X != bombsite.X || this.pathFinders[index].End.Y != bombsite.Y)
                            {
                                this.pathFinders[index] = new PathFinder(
                                   this.model.Map,
                                   new System.Drawing.Point((int)bot.X, (int)bot.Y),
                                   bombsite);
                                this.pathFinders[index].ConstructMap();
                                return;
                            }

                            if (this.pathFinders[index].PathComplete)
                            {
                                bot.HasBombInHands = true;
                                this.HandleAttack(bot);
                                return;
                            }
                        }
                        else if (this.model.Bomb.IsOnGround)
                        {
                            if (this.pathFinders[index].End.X != (int)this.model.Bomb.X || this.pathFinders[index].End.Y != (int)this.model.Bomb.Y)
                            {
                                this.pathFinders[index] = new PathFinder(
                                    this.model.Map,
                                    new System.Drawing.Point((int)bot.X, (int)bot.Y),
                                    bombsite);
                                this.pathFinders[index].ConstructMap();
                                return;
                            }

                            if (this.pathFinders[index].PathComplete)
                            {
                                this.model.Bomb.IsOnGround = false;
                                bot.HasBomb = true;
                                return;
                            }
                        }
                    }
                }

                if (!this.pathFinders[index].PathComplete)
                {
                    int x = (int)Math.Truncate(bot.X + FractionalPartOfNumber(bot.X));
                    int y = (int)Math.Truncate(bot.Y + FractionalPartOfNumber(bot.Y));

                    this.pathFinders[index].X = x;
                    this.pathFinders[index].Y = y;

                    var nextStep = this.pathFinders[index].GetNextStep();
                    double dx = nextStep.X - bot.X;
                    double dy = nextStep.Y - bot.Y;

                    if (dy < 0 && dx > 0)
                    { // moving up and right
                        x = (int)Math.Truncate(bot.X);
                        this.pathFinders[index].X = x;
                        nextStep = this.pathFinders[index].GetNextStep();
                        dx = nextStep.X - bot.X;
                        dy = nextStep.Y - bot.Y;
                    }
                    else if (dy > 0 && dx > 0)
                    { // moving down and right
                        x = (int)Math.Truncate(bot.X);
                        y = (int)Math.Truncate(bot.Y);
                        this.pathFinders[index].X = x;
                        this.pathFinders[index].Y = y;

                        nextStep = this.pathFinders[index].GetNextStep();
                        if (nextStep.X - bot.X >= 0)
                        {
                            dx = nextStep.X - bot.X;
                        }

                        dy = nextStep.Y - bot.Y;
                    }

                    if (dx != 0 && dy != 0)
                    { // ------------------------------------------------------ Diagonal movement
                        bool dxHasSpace = Math.Abs(dx) > Config.StepSize;
                        bool dyHasSpace = Math.Abs(dy) > Config.StepSize;
                        if (dxHasSpace && dyHasSpace)
                        {
                            bot.DX = Math.Round(ClampMagnitude(dx), 8);
                            bot.DY = Math.Round(ClampMagnitude(dy), 8);
                        }
                        else if (dxHasSpace)
                        {
                            bot.DX = dx > 0 ? Config.StepSize : -Config.StepSize;
                            bot.DY = 0; // DY doesn't have space.
                            bot.Y = Math.Round(bot.Y);
                        }
                        else if (dyHasSpace)
                        {
                            bot.DY = dy > 0 ? Config.StepSize : -Config.StepSize;
                            bot.DX = 0; // DX doesn't have space.
                            bot.X = Math.Round(bot.X);
                        }
                        else
                        {
                            if (Distance(this.pathFinders[index].End.X, this.pathFinders[index].End.Y, bot.X, bot.Y) < Config.StepSize * Config.StepSize)
                            {
                                this.pathFinders[index].PathComplete = true;
                            }
                            else
                            {
                                bot.X = Math.Round(bot.X);
                                bot.Y = Math.Round(bot.Y);
                            }
                        }
                    }
                    else if (dx != 0)
                    { // ------------------------------------------------------ X-axis movement
                        if (Math.Abs(dx) > Config.StepSize)
                        {
                            bot.DX = dx > 0 ? Config.StepSize : -Config.StepSize;
                            bot.DY = 0;
                        }
                        else
                        {
                            bot.X = Math.Round(bot.X);
                        }
                    }
                    else if (dy != 0)
                    { // ------------------------------------------------------ Y-axis movement
                        if (Math.Abs(dy) > Config.StepSize)
                        {
                            bot.DY = dy > 0 ? Config.StepSize : -Config.StepSize;
                            bot.DX = 0;
                        }
                        else
                        {
                            bot.Y = Math.Round(bot.Y);
                        }
                    }
                    else
                    { // ------------------------------------------------------ No movement
                        if (Distance(this.pathFinders[index].End.X, this.pathFinders[index].End.Y, bot.X, bot.Y) < Config.StepSize * Config.StepSize)
                        {
                            this.pathFinders[index].PathComplete = true;
                        }
                    }

                    /*bot.DX /= 2;
                    //bot.DY /= 2;

                    if (this.DoesObjectCollidesWithAWall(bot.X + Math.Round(bot.DX, 8), bot.Y + Math.Round(bot.DY, 8)))
                    {
                        ;
                    }*/

                    this.UpdateEntityPos(bot);
                }
            }
        }

        private IList<IEntity> AllEntitiesOfTeam(TeamType team)
        {
            List<Entity> entities = this.model.Bots.Where(x => x.Team == team).ToList();
            if (this.model.Player.Team == team)
            {
                entities.Add(this.model.Player);
            }

            return entities.Cast<IEntity>().ToList();
        }

        private bool CanSeeAnotherEntity(IEntity source, IEntity target, out double angle)
        {
            angle = 0;
            double desiredAngle = 90 + (Math.Atan2(target.Y - source.Y, target.X - source.X) * 180 / Math.PI);

            double radian = (desiredAngle - 90) / 180 * Math.PI;

            Bullet tempBullet = new Bullet(
                source,
                0,
                desiredAngle,
                (0.5 * Math.Cos(radian)) + source.X + 0.5,
                (0.5 * Math.Sin(radian)) + source.Y + 0.5,
                Math.Cos(radian) * 0.1,
                Math.Sin(radian) * 0.1);

            double lastDist = Distance(tempBullet, target);
            double currentDistance = lastDist;

            while (lastDist >= currentDistance)
            {
                tempBullet.ChangeCoordinatesBySpeed();

                if (this.DoesObjectCollidesWithAWall(tempBullet.X, tempBullet.Y, true))
                {
                    return false;
                }

                if (this.DoesObjectCollidesWithAnEntity(tempBullet, out IEntity hitEntity, this.AllEntitiesOfTeam(source.Team).Cast<IMovable>().ToList()))
                {
                    if (hitEntity == target)
                    {
                        angle = desiredAngle;
                        return true;
                    }
                }

                currentDistance = Distance(tempBullet, target);
            }

            return false;
        }

        private void Shoot(IEntity shooter)
        {
            if (shooter == null)
            {
                throw new ArgumentNullException(nameof(shooter), "Shooter shouldn't be null!");
            }

            if (shooter.Weapon == null)
            {
                throw new ArgumentNullException(nameof(shooter.Weapon), "Weapon shouldn't be null!");
            }

            if (!shooter.Weapon.CanShoot(false))
            {
                return;
            }

            SoundEffectType soundEffectType;

            switch (shooter.Weapon.WeaponType)
            {
                case WeaponType.USP:
                    soundEffectType = SoundEffectType.USPShoot;
                    break;
                case WeaponType.Glock:
                    soundEffectType = SoundEffectType.GlockShoot;
                    break;
                default:
                    soundEffectType = SoundEffectType.None;
                    break;
            }

            this.SoundEvent?.Invoke(null, new GameSoundEffectsEventArgs() { SoundEffect = soundEffectType });

            double radian = (shooter.Angle - 90) / 180 * Math.PI;

            this.model.Bullets.Add(new Bullet(
                shooter,
                shooter.Weapon.Damage,
                shooter.Angle,
                (0.5 * Math.Cos(radian)) + shooter.X + 0.5,
                (0.5 * Math.Sin(radian)) + shooter.Y + 0.5,
                Math.Cos(radian) * shooter.Weapon.BulletVelocity,
                Math.Sin(radian) * shooter.Weapon.BulletVelocity));
        }

        private void PlantBomb(IEntity planter)
        {
            if (planter == null)
            {
                throw new ArgumentNullException(nameof(planter), "Planter shouldn't be null!");
            }

            if (this.AtBombsite(planter))
            {
                if (this.model.Bomb.FinishedArming(true))
                {
                    this.SoundEvent?.Invoke(null, new GameSoundEffectsEventArgs() { SoundEffect = SoundEffectType.BombArming, StopSound = true });
                    this.SoundEvent?.Invoke(null, new GameSoundEffectsEventArgs() { SoundEffect = SoundEffectType.BombPlanted });

                    double radian = (planter.Angle - 90) / 180 * Math.PI;

                    this.model.Bomb.X = (0.5 * Math.Cos(radian)) + planter.X;
                    this.model.Bomb.Y = (0.5 * Math.Sin(radian)) + planter.Y;
                    this.model.Bomb.Angle = planter.Angle;

                    this.model.Bomb.IsPlanted = true;
                    planter.HasBomb = false;
                    planter.HasBombInHands = false;
                    planter.Weapon.CanShoot(false);

                    this.roundTimer.Stop();
                    this.model.RoundTime = TimeSpan.FromSeconds(Config.BombTime);
                    this.bombTimer.Start();
                }
                else if (this.model.Bomb.JustStartedArming)
                {
                    this.SoundEvent?.Invoke(null, new GameSoundEffectsEventArgs() { SoundEffect = SoundEffectType.BombArming });
                    this.model.Bomb.JustStartedArming = false;
                }
            }
        }

        private void DefuseBomb(IEntity attacker)
        {
            if (attacker == null)
            {
                throw new ArgumentNullException(nameof(attacker), "Attacker shouldn't be null!");
            }

            if (this.model.Bomb.FinishedDefusing(true))
            {
                this.SoundEvent?.Invoke(null, new GameSoundEffectsEventArgs() { SoundEffect = SoundEffectType.BombPlanted, StopSound = true });
                this.SoundEvent?.Invoke(null, new GameSoundEffectsEventArgs() { SoundEffect = SoundEffectType.BombDefusing, StopSound = true });
                this.SoundEvent?.Invoke(null, new GameSoundEffectsEventArgs() { SoundEffect = SoundEffectType.BombDefused });

                attacker.Weapon.CanShoot(false);

                this.model.Bomb.IsPlanted = false;
                this.bombTimer.Stop();
                this.roundTimer.Stop();
                this.model.RoundTime = new TimeSpan(0);

                this.model.NumberOfWonRoundsByCounterTerrorists++;
                this.NextRound();
            }
            else if (this.model.Bomb.JustStartedDefusing)
            {
                this.SoundEvent?.Invoke(null, new GameSoundEffectsEventArgs() { SoundEffect = SoundEffectType.BombDefusing });
                this.model.Bomb.JustStartedDefusing = false;
            }
        }

        private bool AtBombsite(IEntity entity)
        {
            if (this.bombsite == default)
            {
                for (int x = 0; x < this.model.Map.GetLength(0); x++)
                {
                    for (int y = 0; y < this.model.Map.GetLength(1); y++)
                    {
                        if (this.model.Map[x, y] == NodeType.BombSiteA)
                        {
                            this.bombsite = new Point(x + 0.5, y + 0.5);
                            break;
                        }
                    }
                }
            }

            return (Math.Pow(entity.X - this.bombsite.X, 2) + Math.Pow(entity.Y - this.bombsite.Y, 2)) < Math.Pow(Config.BombSiteSize, 2);
        }

        private void UpdateXAxis()
        {
            this.model.Player.X += Math.Round(this.model.Player.DX, 8);
            this.model.Player.DX = 0;
        }

        private void UpdateYAxis()
        {
            this.model.Player.Y += Math.Round(this.model.Player.DY, 8);
            this.model.Player.DY = 0;
        }

        private bool IsEntityMovable(ref double x, ref double y, Direction direction)
        {
            switch (direction)
            {
                case Direction.Up:
                    y -= Config.StepSize;
                    break;
                case Direction.Right:
                    x += Config.StepSize;
                    break;
                case Direction.Down:
                    y += Config.StepSize;
                    break;
                case Direction.Left:
                    x -= Config.StepSize;
                    break;
            }

            return !this.DoesObjectCollidesWithAWall(x, y);
        }

        private bool OutOfMapBounds(double xCord, double yCord)
        {
            return xCord < 0 || yCord < 0 || xCord > this.Model.Map.GetLength(0) - Config.StepSize || yCord > this.Model.Map.GetLength(1) - Config.StepSize;
        }

        private bool DoesObjectCollidesWithAWall(double x, double y, bool checkForBullet = false)
        {
            if (this.OutOfMapBounds(x, y) ||
                this.model.Map[(int)Math.Truncate(x), (int)Math.Truncate(y)] == NodeType.Wall)
            {
                return true;
            }

            if (checkForBullet)
            {
                return false;
            }

            if (this.OutOfMapBounds(x + 1, y) ||
                this.model.Map[(int)Math.Truncate(x + FractionalPartOfNumber(x)), (int)Math.Truncate(y)] == NodeType.Wall ||
                this.OutOfMapBounds(x, y + 1) ||
                this.model.Map[(int)Math.Truncate(x), (int)Math.Truncate(y + FractionalPartOfNumber(y))] == NodeType.Wall ||
                this.OutOfMapBounds(x + 1, y + 1) ||
                this.model.Map[(int)Math.Truncate(x + FractionalPartOfNumber(x)), (int)Math.Truncate(y + FractionalPartOfNumber(y))] == NodeType.Wall)
            {
                return true;
            }

            return false;
        }

        private bool DoesObjectCollidesWithAnEntity(IMovable entity, out IEntity hit, IMovable ignored = null)
        {
            double xOffset = 1;
            double yOffset = 1;

            if (entity is IBullet)
            {
                double radian = (entity.Angle - 90) / 180 * Math.PI;

                xOffset = Config.BulletWidth * Math.Cos(radian);
                yOffset = Config.BulletHeight * Math.Cos(radian);
            }

            for (int i = 0; i <= this.model.Bots.Count; i++)
            {
                var item = i == this.model.Bots.Count ? this.model.Player : this.model.Bots[i];

                if (item == ignored || item.Health == 0)
                {
                    continue;
                }

                if (entity.X <= item.X + 1 &&
                   entity.X + xOffset >= item.X &&
                   entity.Y <= item.Y + 1 &&
                   entity.Y + yOffset >= item.Y)
                {
                    hit = item;
                    return true;
                }
            }

            hit = null;
            return false;
        }

        private bool DoesObjectCollidesWithAnEntity(IMovable entity, out IEntity hit, IList<IMovable> ignoredEntities)
        {
            double xOffset = 1;
            double yOffset = 1;

            if (entity is IBullet)
            {
                double radian = (entity.Angle - 90) / 180 * Math.PI;

                xOffset = Config.BulletWidth * Math.Cos(radian);
                yOffset = Config.BulletHeight * Math.Cos(radian);
            }

            for (int i = 0; i <= this.model.Bots.Count; i++)
            {
                var item = i == this.model.Bots.Count ? this.model.Player : this.model.Bots[i];

                if (item.Health == 0 || ignoredEntities.Contains(item))
                {
                    continue;
                }

                if (entity.X <= item.X + 1 &&
                   entity.X + xOffset >= item.X &&
                   entity.Y <= item.Y + 1 &&
                   entity.Y + yOffset >= item.Y)
                {
                    hit = item;
                    return true;
                }
            }

            hit = null;
            return false;
        }

        private void InitEntities(List<Point> spawnPoints, TeamType teamType, bool newGame = true)
        {
            var allEntitites = !newGame ? this.AllEntitiesOfTeam(teamType).ToList() : null;
            if (!newGame)
            {
                allEntitites.ForEach((x) =>
                {
                    x.DX = 0;
                    x.DY = 0;
                    x.Health = Config.MaxHealth;
                    x.HasBomb = false;
                    x.HasBombInHands = false;
                });
            }

            byte entitiesLeftToIntizie = teamType == TeamType.CounterTerrorist ? Config.NumberOfCounterTerrorists : Config.NumberOfTerrorists;

            bool bombHasBeenGivenOut = false;
            bool playerHasBeenInited = this.model.Player != null && (newGame || this.model.Player.Team != teamType);

            int counter = !newGame ? allEntitites.Count - 1 : 0;

            while (entitiesLeftToIntizie > 0 && spawnPoints.Count > 0)
            {
                var index = Rnd.Next(0, spawnPoints.Count);

                if (newGame)
                {
                    var model = teamType == TeamType.CounterTerrorist ?
                    (ModelType)Rnd.Next((int)ModelType.CounterTerroristSkin1, (int)ModelType.CounterTerroristSkin4 + 1) :
                    (ModelType)Rnd.Next((int)ModelType.TerroristSkin1, (int)ModelType.TerroristSkin4 + 1);

                    Entity newEntity = new Entity()
                    {
                        Weapon = new Weapon()
                        {
                            WeaponType = teamType == TeamType.CounterTerrorist ? WeaponType.USP : WeaponType.Glock,
                            Damage = teamType == TeamType.CounterTerrorist ? (byte)20 : (byte)15,
                            AmmoCapacity = teamType == TeamType.CounterTerrorist ? (byte)12 : (byte)20,
                            BulletVelocity = teamType == TeamType.CounterTerrorist ? 0.25 : 0.2,
                            CoolDown = 20,
                            Price = 200,
                        },
                        Model = model,
                        X = spawnPoints[index].X,
                        Y = spawnPoints[index].Y,
                        Team = teamType,
                        Health = Config.MaxHealth,
                    };

                    if (teamType == TeamType.Terrorist && !bombHasBeenGivenOut)
                    {
                        newEntity.HasBomb = true;
                        this.model.Bomb = new Bomb()
                        {
                            ArmingTime = Config.BombArmingTime,
                        };
                        bombHasBeenGivenOut = true;
                    }

                    if (!playerHasBeenInited)
                    {
                        this.Model.Player = newEntity;
                        playerHasBeenInited = true;
                    }
                    else
                    {
                        this.Model.Bots.Add(newEntity);
                    }
                }
                else
                {
                    allEntitites[counter].X = spawnPoints[index].X;
                    allEntitites[counter].Y = spawnPoints[index].Y;

                    if (teamType == TeamType.Terrorist && !bombHasBeenGivenOut)
                    {
                        allEntitites[counter].HasBomb = true;
                        this.model.Bomb = new Bomb()
                        {
                            ArmingTime = Config.BombArmingTime,
                        };
                        bombHasBeenGivenOut = true;
                    }

                    counter--;
                }

                spawnPoints.RemoveAt(index);
                entitiesLeftToIntizie--;
            }
        }
    }
}
