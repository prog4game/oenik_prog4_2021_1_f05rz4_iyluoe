﻿// <copyright file="GlobalSuppressions.cs" company="Klavaro">
// Copyright (c) Klavaro. All rights reserved.
// </copyright>

// This file is used by Code Analysis to maintain SuppressMessage
// attributes that are applied to this project.
// Project-level suppressions either have no target or are given
// a specific target and scoped to a namespace, type, member, etc.
using System.Diagnostics.CodeAnalysis;

[assembly: SuppressMessage("Design", "CA1002:Do not expose generic lists", Justification = "It has to be.", Scope = "member", Target = "~M:Wpf2DShooter.GameLogicNS.IGameLogic.HandleBulletsAndReturnListOfBulletsThatHitSomething(System.Collections.Generic.List{Wpf2DShooter.GameModelNS.IBullet}@,System.Collections.Generic.List{Wpf2DShooter.GameModelNS.IBullet}@)")]
[assembly: SuppressMessage("Performance", "CA1814:Prefer jagged arrays over multidimensional", Justification = "I'm fine with matrixes.", Scope = "member", Target = "~M:Wpf2DShooter.GameLogicNS.GameLogic.InitGame(System.String)")]
[assembly: SuppressMessage("Security", "CA5394:Do not use insecure randomness", Justification = "This is not a banking application.", Scope = "member", Target = "~M:Wpf2DShooter.GameLogicNS.GameLogic.InitEntities(System.Collections.Generic.List{System.Windows.Point},Wpf2DShooter.GameModelNS.TeamType,System.Boolean)")]
[assembly: SuppressMessage("Security", "CA5394:Do not use insecure randomness", Justification = "This is not a banking application.", Scope = "member", Target = "~M:Wpf2DShooter.GameLogicNS.GameLogic.HandleBulletsAndReturnListOfBulletsThatHitSomething(System.Collections.Generic.List{Wpf2DShooter.GameModelNS.IBullet}@,System.Collections.Generic.List{Wpf2DShooter.GameModelNS.IBullet}@)")]
[assembly: SuppressMessage("Security", "CA5394:Do not use insecure randomness", Justification = "This is not a banking application.", Scope = "member", Target = "~M:Wpf2DShooter.GameLogicNS.GameLogic.GetPositionOnMap(System.Collections.Generic.List{System.Drawing.Point},Wpf2DShooter.GameModelNS.TeamType)~System.Drawing.Point")]
[assembly: SuppressMessage("Performance", "CA1814:Prefer jagged arrays over multidimensional", Justification = "I'm fine with matrixes.", Scope = "member", Target = "~F:Wpf2DShooter.GameLogicNS.PathFinder.map")]
[assembly: SuppressMessage("Performance", "CA1814:Prefer jagged arrays over multidimensional", Justification = "I'm fine with matrixes", Scope = "member", Target = "~F:Wpf2DShooter.GameLogicNS.PathFinder.nodeMap")]
[assembly: SuppressMessage("Performance", "CA1814:Prefer jagged arrays over multidimensional", Justification = "I'm fine with matrixes", Scope = "member", Target = "~M:Wpf2DShooter.GameLogicNS.PathFinder.ConstructMap")]
[assembly: SuppressMessage("Performance", "CA1814:Prefer jagged arrays over multidimensional", Justification = "I'm fine with matrixes", Scope = "member", Target = "~M:Wpf2DShooter.GameLogicNS.PathFinder.#ctor(Wpf2DShooter.GameModelNS.NodeType[,],System.Drawing.Point,System.Drawing.Point)")]
[assembly: SuppressMessage("Security", "CA5394:Do not use insecure randomness", Justification = "This is not a banking application.", Scope = "member", Target = "~M:Wpf2DShooter.GameLogicNS.GameLogic.BotMovement(Wpf2DShooter.GameModelNS.Entity)")]
