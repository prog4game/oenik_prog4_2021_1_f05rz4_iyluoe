﻿// <copyright file="IGameLogic.cs" company="Klavaro">
// Copyright (c) Klavaro. All rights reserved.
// </copyright>

namespace Wpf2DShooter.GameLogicNS
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;
    using Wpf2DShooter.GameModelNS;

    /// <summary>
    /// Defines the components needed for the game's logic.
    /// </summary>
    public interface IGameLogic : IDisposable
    {
        /// <summary>
        /// Gets or Sets : An <see cref="IGameModel"/> implementation.
        /// </summary>
        IGameModel Model { get; set; }

        /// <summary>
        /// Sets an <see cref="EventHandler"/> implementation for handling sound effects.
        /// </summary>
        /// <param name="eventHandler">The <see cref="EventHandler"/> for handling the sounds effects.</param>
        public void AddSoundEventHandler(EventHandler<GameSoundEffectsEventArgs> eventHandler);

        /// <summary>
        /// Initializes the current <see cref="IGameModel"/> implementation according to the given resource.
        /// </summary>
        /// <param name="mapSrc">The resource representing the gamemap found in the executing assembly.</param>
        void InitGame(string mapSrc);

        /// <summary>
        /// Calculates velocity for the axieses of the player.
        /// </summary>
        /// <param name="direction">The direction to move the player in.</param>
        /// <returns>The result of the moving.</returns>
        bool SetPlayerVelocity(Direction direction);

        /// <summary>
        /// Moves the entity on the game map.
        /// </summary>
        /// <param name="entity">Entity to be moved.</param>
        void UpdateEntityPos(IMovable entity);

        /// <summary>
        /// Calculates the position of the entity on the screen.
        /// </summary>
        /// <param name="entity">Entity used for the calculation.</param>
        /// <returns>The position of the entity on the screen.</returns>
        Point PositionOfEntityOnScreen(IMovable entity);

        /// <summary>
        /// Handles an attacking intention.
        /// </summary>
        /// <param name="attacker">The entity trying to attack.</param>
        void HandleAttack(IEntity attacker);

        /// <summary>
        /// Tries to switch the bomb or to the weapon.
        /// </summary>
        /// <param name="entity">The entity that tries switching.</param>
        /// <returns>If switching was succesful.</returns>
        bool SwitchToBomb(IEntity entity);

        /// <summary>
        /// Handles the flying bullets in the game, and returns those in a list which either hit a wall or an entity.
        /// </summary>
        /// <param name="wall">List of bullets which hit a wall.</param>
        /// <param name="entities">List of bullets which hit an entity.</param>
        void HandleBulletsAndReturnListOfBulletsThatHitSomething(out List<IBullet> wall, out List<IBullet> entities);

        /// <summary>
        /// Handles the cooldowns of every entities weapon.
        /// </summary>
        void HandleWeaponCoolDowns();

        /// <summary>
        /// Moves the bots on the map.
        /// </summary>
        void MoveBots();
    }
}
