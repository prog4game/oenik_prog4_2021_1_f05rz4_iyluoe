﻿// <copyright file="GlobalSuppressions.cs" company="Klavaro">
// Copyright (c) Klavaro. All rights reserved.
// </copyright>

// This file is used by Code Analysis to maintain SuppressMessage
// attributes that are applied to this project.
// Project-level suppressions either have no target or are given
// a specific target and scoped to a namespace, type, member, etc.
using System.Diagnostics.CodeAnalysis;

[assembly: SuppressMessage("Performance", "CA1819:Properties should not return arrays", Justification = "But it's an array.", Scope = "member", Target = "~P:Wpf2DShooter.GameModelNS.GameModel.Map")]
[assembly: SuppressMessage("Usage", "CA2227:Collection properties should be read only", Justification = "It has to be set.", Scope = "member", Target = "~P:Wpf2DShooter.GameModelNS.GameModel.Bullets")]
[assembly: SuppressMessage("Usage", "CA2227:Collection properties should be read only", Justification = "It has to be set.", Scope = "member", Target = "~P:Wpf2DShooter.GameModelNS.GameModel.Bots")]
[assembly: SuppressMessage("Performance", "CA1814:Prefer jagged arrays over multidimensional", Justification = "I'm fine with matrixes.", Scope = "member", Target = "~P:Wpf2DShooter.GameModelNS.IGameModel.Map")]
[assembly: SuppressMessage("Performance", "CA1819:Properties should not return arrays", Justification = "But it's an array.", Scope = "member", Target = "~P:Wpf2DShooter.GameModelNS.IGameModel.Map")]
[assembly: SuppressMessage("Design", "CA1002:Do not expose generic lists", Justification = "It has to be.", Scope = "member", Target = "~P:Wpf2DShooter.GameModelNS.IGameModel.Bullets")]
[assembly: SuppressMessage("Usage", "CA2227:Collection properties should be read only", Justification = "It has to be set.", Scope = "member", Target = "~P:Wpf2DShooter.GameModelNS.IGameModel.Bullets")]
[assembly: SuppressMessage("Design", "CA1002:Do not expose generic lists", Justification = "It has to be.", Scope = "member", Target = "~P:Wpf2DShooter.GameModelNS.IGameModel.Bots")]
[assembly: SuppressMessage("Usage", "CA2227:Collection properties should be read only", Justification = "It has to be set.", Scope = "member", Target = "~P:Wpf2DShooter.GameModelNS.IGameModel.Bots")]
[assembly: SuppressMessage("Performance", "CA1814:Prefer jagged arrays over multidimensional", Justification = "I'm fine with matrixes.", Scope = "member", Target = "~F:Wpf2DShooter.GameModelNS.GameModel.map")]
