﻿// <copyright file="IGameModel.cs" company="Klavaro">
// Copyright (c) Klavaro. All rights reserved.
// </copyright>

namespace Wpf2DShooter.GameModelNS
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;
    using Wpf2DShooter.GameModelNS;

    /// <summary>
    /// Declares which components are needed to represent the game's state.
    /// </summary>
    public interface IGameModel
    {
        /// <summary>
        /// Gets or Sets : the matrix representing the map of the game.
        /// </summary>
        NodeType[,] Map { get; set; }

        /// <summary>
        /// Gets or Sets : The bomb of the entity.
        /// </summary>
        public IBomb Bomb { get; set; }

        /// <summary>
        /// Gets or Sets : The remaining time of the round.
        /// </summary>
        public TimeSpan RoundTime { get; set; }

        /// <summary>
        /// Gets or Sets : The number of won rounds by counter terrorists.
        /// </summary>
        byte NumberOfWonRoundsByCounterTerrorists { get; set; }

        /// <summary>
        /// Gets or Sets : The number of won rounds by terrorists.
        /// </summary>
        byte NumberOfWonRoundsByTerrorists { get; set; }

        /// <summary>
        /// Gets or Sets : The chosen team of the player.
        /// </summary>
        TeamType ChosenTeam { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether : It's freeze time or not.
        /// </summary>
        public bool FreezeTime { get; set; }

        /// <summary>
        /// Gets or Sets : The list of bullets in the game.
        /// </summary>
        List<Bullet> Bullets { get; set; }

        /// <summary>
        /// Gets or Sets : The list of bots in the game.
        /// </summary>
        List<Entity> Bots { get; set; }

        /// <summary>
        /// Gets or Sets : The player of the game.
        /// </summary>
        Entity Player { get; set; }

        /// <summary>
        /// Gets : The width of the current game session.
        /// </summary>
        double GameWidth { get; }

        /// <summary>
        /// Gets : The height of the current game session.
        /// </summary>
        double GameHeight { get; }

        /// <summary>
        /// Gets or Sets : The width size of one tile in the game.
        /// </summary>
        double TileSizeWidth { get; set; }

        /// <summary>
        /// Gets or Sets : The height size of one tile in the game.
        /// </summary>
        double TileSizeHeight { get; set; }
    }
}
