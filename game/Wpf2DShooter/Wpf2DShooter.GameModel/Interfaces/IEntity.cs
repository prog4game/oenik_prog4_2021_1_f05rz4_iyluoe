﻿// <copyright file="IEntity.cs" company="Klavaro">
// Copyright (c) Klavaro. All rights reserved.
// </copyright>

namespace Wpf2DShooter.GameModelNS
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Defines an entity in the game.
    /// Extends the <see cref="IMovable"/> interface.
    /// </summary>
    public interface IEntity : IMovable
    {
        /// <summary>
        /// Gets or Sets : The model (skin) of the entity.
        /// </summary>
        ModelType Model { get; set; }

        /// <summary>
        /// Gets or Sets : The weapon of the entity.
        /// </summary>
        IWeapon Weapon { get; set; }

        /// <summary>
        /// Gets or Sets a value indicating whether : The entity has a bomb or not.
        /// </summary>
        bool HasBomb { get; set; }

        /// <summary>
        /// Gets or Sets a value indicating whether : The entity has a bomb in its hand or not.
        /// </summary>
        bool HasBombInHands { get; set; }

        /// <summary>
        /// Gets or Sets: The health of the entity.
        /// </summary>
        short Health { get; set; }

        /// <summary>
        /// Gets or Sets: The team of the entity.
        /// </summary>
        TeamType Team { get; set; }
    }
}
