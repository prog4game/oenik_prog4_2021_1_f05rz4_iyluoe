﻿// <copyright file="IBomb.cs" company="Klavaro">
// Copyright (c) Klavaro. All rights reserved.
// </copyright>

namespace Wpf2DShooter.GameModelNS
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Describes which data are required to represent a bomb in the game.
    /// Extends the <see cref="IMovable"/> interface.
    /// </summary>
    public interface IBomb : IMovable
    {
        /// <summary>
        /// Gets or Sets a value indicating whether: The bomb is planted or not.
        /// </summary>
        public bool IsPlanted { get; set; }

        /// <summary>
        /// Gets or Sets a value indicating whether: The bomb is on the ground or not.
        /// </summary>
        public bool IsOnGround { get; set; }

        /// <summary>
        /// Gets or Sets a value indicating whether: The bomb's arming just been started or not.
        /// </summary>
        public bool JustStartedArming { get; set; }

        /// <summary>
        /// Gets or Sets a value indicating whether: The bomb's arming just been stopped or not.
        /// </summary>
        public bool JustStoppedArming { get; set; }

        /// <summary>
        /// Gets or Sets a value indicating whether: The bomb's defusing just been started or not.
        /// </summary>
        public bool JustStartedDefusing { get; set; }

        /// <summary>
        /// Gets or Sets a value indicating whether: The bomb's defusing just been stopped or not.
        /// </summary>
        public bool JustStoppedDefusing { get; set; }

        /// <summary>
        /// Gets : The type of the weapon.
        /// </summary>
        public WeaponType WeaponType { get; }

        /// <summary>
        /// Gets or Sets : The required time arming the bomb.
        /// </summary>
        public byte ArmingTime { get; set; }

        /// <summary>
        /// Gets or Sets : The required time to defuse the bomb.
        /// </summary>
        public byte DefuseTime { get; set; }

        /// <summary>
        /// Tells if bomb's arming was done.
        /// </summary>
        /// <param name="isCurrentlyArming">True if the bomb's being armed.</param>
        /// <returns>True if bomb's arming was done.</returns>
        public bool FinishedArming(bool isCurrentlyArming);

        /// <summary>
        /// Tells if bomb's defuse was done.
        /// </summary>
        /// <param name="isCurrentlyDefusing">True if the bomb's being defused.</param>
        /// <returns>True if bomb's defusing was done.</returns>
        public bool FinishedDefusing(bool isCurrentlyDefusing);
    }
}
