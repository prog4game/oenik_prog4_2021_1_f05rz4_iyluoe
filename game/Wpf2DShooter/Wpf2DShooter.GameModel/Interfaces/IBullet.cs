﻿// <copyright file="IBullet.cs" company="Klavaro">
// Copyright (c) Klavaro. All rights reserved.
// </copyright>

namespace Wpf2DShooter.GameModelNS
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Defines a bullet in the game.
    /// Extends the <see cref="IMovable"/> interface.
    /// </summary>
    public interface IBullet : IMovable
    {
        /// <summary>
        /// Gets or Sets : The owner entity of the bullet.
        /// </summary>
        IEntity Owner { get; set; }

        /// <summary>
        ///  Gets or Sets a value indicating whether : The bullet was lethal.
        /// </summary>
        bool WasLethal { get; set; }

        /// <summary>
        /// Gets or Sets : The damage of the bullet.
        /// </summary>
        double Damage { get; set; }

        /// <summary>
        /// Changes the object position by the speed vector.
        /// </summary>
        void ChangeCoordinatesBySpeed();
    }
}
