﻿// <copyright file="IWeapon.cs" company="Klavaro">
// Copyright (c) Klavaro. All rights reserved.
// </copyright>

namespace Wpf2DShooter.GameModelNS
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Describes which data are required to represent a weapon in the game.
    /// </summary>
    public interface IWeapon
    {
        /// <summary>
        /// Gets or Sets : The type of the weapon.
        /// </summary>
        public WeaponType WeaponType { get; set; }

        /// <summary>
        /// Gets or Sets : The damage of the weapon.
        /// </summary>
        public byte Damage { get; set; }

        /// <summary>
        /// Gets or Sets : The ammocapacity of the weapon.
        /// </summary>
        public byte AmmoCapacity { get; set; }

        /// <summary>
        /// Gets or Sets : The velocity of a bullet of the weapon.
        /// </summary>
        public double BulletVelocity { get; set; }

        /// <summary>
        /// Gets or Sets : The cooldown of the weapon after shooting it.
        /// </summary>
        public byte CoolDown { get; set; }

        /// <summary>
        /// Gets or Sets : The price of the weapon.
        /// </summary>
        public ushort Price { get; set; }

        /// <summary>
        /// Tells if weapon can or can't be shot because of the cooldown.
        /// </summary>
        /// <param name="onlyHandled">Should be true if the counter only needs to be incremented because the weapon's already shot.</param>
        /// <returns>True if the weapon can be shot.</returns>
        public bool CanShoot(bool onlyHandled = true);
    }
}
