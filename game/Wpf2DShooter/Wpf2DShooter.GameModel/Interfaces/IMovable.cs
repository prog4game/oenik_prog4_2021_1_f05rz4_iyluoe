﻿// <copyright file="IMovable.cs" company="Klavaro">
// Copyright (c) Klavaro. All rights reserved.
// </copyright>

namespace Wpf2DShooter.GameModelNS
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Describes how a movable object should behave in the game.
    /// </summary>
    public interface IMovable
    {
        /// <summary>
        /// Gets or Sets : The angle in degrees.
        /// </summary>
        double Angle { get; set; }

        /// <summary>
        /// Gets or sets the coordinate of the object on the x-axis.
        /// </summary>
        double X { get; set; }

        /// <summary>
        /// Gets or sets the coordinate of the object on the y-axis.
        /// </summary>
        double Y { get; set; }

        /// <summary>
        /// Gets or sets the x component of the speed vector.
        /// </summary>
        double DX { get; set; }

        /// <summary>
        /// Gets or sets the x component of the speed vector.
        /// </summary>
        double DY { get; set; }
    }
}
