﻿// <copyright file="WeaponType.cs" company="Klavaro">
// Copyright (c) Klavaro. All rights reserved.
// </copyright>

namespace Wpf2DShooter.GameModelNS
{
    /// <summary>
    /// Describes the different type of weapons in the game.
    /// </summary>
    public enum WeaponType
    {
        /// <summary>
        /// Because stylecop has nothing better to do.
        /// </summary>
        None,

        /// <summary>
        /// The USP handgun.
        /// </summary>
        USP,

        /// <summary>
        /// The Glock handgun.
        /// </summary>
        Glock,

        /// <summary>
        /// The Bomb.
        /// </summary>
        Bomb,
    }
}