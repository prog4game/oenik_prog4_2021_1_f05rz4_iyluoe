﻿// <copyright file="Weapon.cs" company="Klavaro">
// Copyright (c) Klavaro. All rights reserved.
// </copyright>

namespace Wpf2DShooter.GameModelNS
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// An <see cref="IWeapon"/> implementation.
    /// </summary>
    public class Weapon : IWeapon
    {
        private WeaponType weaponType;
        private byte damage;
        private byte ammoCapacity;
        private double bulletVelocity;
        private byte coolDown;
        private ushort price;

        private byte counter;

        /// <inheritdoc/>
        public WeaponType WeaponType { get => this.weaponType; set => this.weaponType = value; }

        /// <inheritdoc/>
        public byte Damage { get => this.damage; set => this.damage = value; }

        /// <inheritdoc/>
        public byte AmmoCapacity { get => this.ammoCapacity; set => this.ammoCapacity = value; }

        /// <inheritdoc/>
        public double BulletVelocity { get => this.bulletVelocity; set => this.bulletVelocity = value; }

        /// <inheritdoc/>
        public byte CoolDown { get => this.coolDown; set => this.coolDown = value; }

        /// <inheritdoc/>
        public ushort Price { get => this.price; set => this.price = value; }

        /// <inheritdoc/>
        public bool CanShoot(bool onlyHandled = true)
        {
            if (this.counter == 0)
            {
                if (!onlyHandled)
                {
                    this.counter++;
                }

                return true;
            }

            this.counter = (byte)((this.counter + 1) % this.coolDown);
            return false;
        }
    }
}
