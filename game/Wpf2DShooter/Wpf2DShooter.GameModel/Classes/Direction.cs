﻿// <copyright file="Direction.cs" company="Klavaro">
// Copyright (c) Klavaro. All rights reserved.
// </copyright>

namespace Wpf2DShooter.GameModelNS
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Represents the facing direction of the in-game elements.
    /// </summary>
    public enum Direction
    {
        /// <summary>
        /// Direction facing upwards.
        /// </summary>
        Up,

        /// <summary>
        /// Direction facing right.
        /// </summary>
        Right,

        /// <summary>
        /// Direction facing downwards.
        /// </summary>
        Down,

        /// <summary>
        /// Direction facing left.
        /// </summary>
        Left,
    }
}
