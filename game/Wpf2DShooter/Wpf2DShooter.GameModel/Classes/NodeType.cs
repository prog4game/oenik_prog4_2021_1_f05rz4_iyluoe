﻿// <copyright file="NodeType.cs" company="Klavaro">
// Copyright (c) Klavaro. All rights reserved.
// </copyright>

namespace Wpf2DShooter.GameModelNS
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Describes the different type of nodes on the map.
    /// </summary>
    public enum NodeType
    {
        /// <summary>
        /// Empty
        /// </summary>
        None,

        /// <summary>
        /// Wall
        /// </summary>
        Wall,

        /// <summary>
        /// Road
        /// </summary>
        Road,

        /// <summary>
        /// Bombsite on A
        /// </summary>
        BombSiteA,

        /// <summary>
        /// Bombiste on B
        /// </summary>
        BombSiteB,
    }
}
