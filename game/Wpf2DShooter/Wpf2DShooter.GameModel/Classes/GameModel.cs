﻿// <copyright file="GameModel.cs" company="Klavaro">
// Copyright (c) Klavaro. All rights reserved.
// </copyright>

namespace Wpf2DShooter.GameModelNS
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;

    /// <summary>
    /// An <see cref="IGameModel"/> implementation.
    /// </summary>
    public class GameModel : IGameModel
    {
        private NodeType[,] map;
        private IBomb bomb;
        private TimeSpan roundTime;
        private byte numberOfWonRoundsByCounterTerrorists;
        private byte numberOfWonRoundsByTerrorists;
        private TeamType chosenTeam;
        private bool freezeTime;
        private List<Bullet> bullets;
        private List<Entity> bots;
        private Entity player;
        private double gameWidth;
        private double gameHeight;
        private double tileSizeWidth;
        private double tileSizeHeight;

        /// <summary>
        /// Initializes a new instance of the <see cref="GameModel"/> class.
        /// </summary>
        /// <param name="width">The width of the game to be set.</param>
        /// <param name="height">The height of the game to be set.</param>
        public GameModel(double width, double height)
        {
            this.bullets = new List<Bullet>();
            this.bots = new List<Entity>();

            this.gameWidth = width;
            this.gameHeight = height;
        }

        /// <inheritdoc/>
        public NodeType[,] Map { get => this.map; set => this.map = value; }

        /// <inheritdoc/>
        public IBomb Bomb { get => this.bomb; set => this.bomb = value; }

        /// <inheritdoc/>
        public TimeSpan RoundTime { get => this.roundTime; set => this.roundTime = value; }

        /// <inheritdoc/>
        public byte NumberOfWonRoundsByCounterTerrorists { get => this.numberOfWonRoundsByCounterTerrorists; set => this.numberOfWonRoundsByCounterTerrorists = value; }

        /// <inheritdoc/>
        public byte NumberOfWonRoundsByTerrorists { get => this.numberOfWonRoundsByTerrorists; set => this.numberOfWonRoundsByTerrorists = value; }

        /// <inheritdoc/>
        public TeamType ChosenTeam { get => this.chosenTeam; set => this.chosenTeam = value; }

        /// <inheritdoc/>
        public bool FreezeTime { get => this.freezeTime; set => this.freezeTime = value; }

        /// <inheritdoc/>
        public List<Bullet> Bullets { get => this.bullets; set => this.bullets = value; }

        /// <inheritdoc/>
        public List<Entity> Bots { get => this.bots; set => this.bots = value; }

        /// <inheritdoc/>
        public Entity Player { get => this.player; set => this.player = value; }

        /// <inheritdoc/>
        public double GameWidth { get => this.gameWidth; }

        /// <inheritdoc/>
        public double GameHeight { get => this.gameHeight; }

        /// <inheritdoc/>
        public double TileSizeWidth { get => this.tileSizeWidth; set => this.tileSizeWidth = value; }

        /// <inheritdoc/>
        public double TileSizeHeight { get => this.tileSizeHeight; set => this.tileSizeHeight = value; }
    }
}
