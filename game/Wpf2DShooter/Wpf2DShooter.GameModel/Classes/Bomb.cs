﻿// <copyright file="Bomb.cs" company="Klavaro">
// Copyright (c) Klavaro. All rights reserved.
// </copyright>

namespace Wpf2DShooter.GameModelNS
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// An <see cref="IBomb"/> implementation.
    /// </summary>
    public class Bomb : IBomb
    {
        private bool isPlanted;
        private bool isOnGround;
        private bool justStartedArming;
        private bool justStoppedArming;
        private bool justStartedDefusing;
        private bool justStoppedDefusing;
        private double x;
        private double y;
        private double angle;
        private byte armingTime;
        private byte defuseTime;

        private byte armCounter;
        private byte defCounter;

        /// <inheritdoc/>
        public bool IsPlanted { get => this.isPlanted; set => this.isPlanted = value; }

        /// <inheritdoc/>
        public bool IsOnGround { get => this.isOnGround; set => this.isOnGround = value; }

        /// <inheritdoc/>
        public bool JustStartedArming { get => this.justStartedArming; set => this.justStartedArming = value; }

        /// <inheritdoc/>
        public bool JustStoppedArming { get => this.justStoppedArming; set => this.justStoppedArming = value; }

        /// <inheritdoc/>
        public bool JustStartedDefusing { get => this.justStartedDefusing; set => this.justStartedDefusing = value; }

        /// <inheritdoc/>
        public bool JustStoppedDefusing { get => this.justStoppedDefusing; set => this.justStoppedDefusing = value; }

        /// <inheritdoc/>
        public double X { get => this.x; set => this.x = value; }

        /// <inheritdoc/>
        public double Y { get => this.y; set => this.y = value; }

        /// <inheritdoc/>
        public double Angle { get => this.angle; set => this.angle = value; }

        /// <inheritdoc/>
        public WeaponType WeaponType => WeaponType.Bomb;

        /// <inheritdoc/>
        public byte ArmingTime { get => this.armingTime; set => this.armingTime = value; }

        /// <inheritdoc/>
        public byte DefuseTime { get => this.defuseTime; set => this.defuseTime = value; }

        /// <inheritdoc/>
        public double DX { get => 0; set => throw new NotImplementedException(); }

        /// <inheritdoc/>
        public double DY { get => 0; set => throw new NotImplementedException(); }

        /// <inheritdoc/>
        public bool FinishedArming(bool isCurrentlyArming)
        {
            if (!isCurrentlyArming)
            {
                if (this.armCounter != 0)
                {
                    this.justStoppedArming = true;
                }

                this.armCounter = 0;
                this.justStartedArming = false;
                return false;
            }

            if (this.armCounter == 0)
            {
                this.justStartedArming = true;
            }

            if (++this.armCounter == this.armingTime)
            {
                this.armCounter = 0;
                return true;
            }

            return false;
        }

        /// <inheritdoc/>
        public bool FinishedDefusing(bool isCurrentlyDefusing)
        {
            if (!isCurrentlyDefusing)
            {
                if (this.defCounter != 0)
                {
                    this.justStoppedDefusing = true;
                }

                this.defCounter = 0;
                this.justStartedDefusing = false;
                return false;
            }

            if (this.defCounter == 0)
            {
                this.justStartedDefusing = true;
            }

            if (++this.defCounter == this.defuseTime)
            {
                this.defCounter = 0;
                return true;
            }

            return false;
        }
    }
}
