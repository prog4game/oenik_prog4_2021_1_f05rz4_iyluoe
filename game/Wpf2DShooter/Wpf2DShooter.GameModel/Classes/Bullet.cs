﻿// <copyright file="Bullet.cs" company="Klavaro">
// Copyright (c) Klavaro. All rights reserved.
// </copyright>

namespace Wpf2DShooter.GameModelNS
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// An <see cref="IBullet"/> bullet implementation.
    /// </summary>
    public class Bullet : IBullet
    {
        private IEntity owner;
        private bool waslethal;
        private double damage;
        private double angle;
        private double x;
        private double y;
        private double dx;
        private double dy;

        /// <summary>
        /// Initializes a new instance of the <see cref="Bullet"/> class.
        /// </summary>
        /// <param name="owner">The owner object of the object.</param>
        /// <param name="damage">The damage of the object.</param>
        /// <param name="angle">The angle of the object.</param>
        /// <param name="x">The x coordinate of the object.</param>
        /// <param name="y">The y coordinate of the object.</param>
        /// <param name="speedX">The x component of the speed the object is moving at.</param>
        /// <param name="speedY">The y component of the speed the object is moving at.</param>
        public Bullet(IEntity owner, double damage, double angle, double x, double y, double speedX, double speedY)
        {
            this.owner = owner;
            this.damage = damage;
            this.angle = angle;
            this.x = x;
            this.y = y;
            this.dx = speedX;
            this.dy = speedY;
        }

        /// <inheritdoc/>
        public IEntity Owner { get => this.owner; set => this.owner = value; }

        /// <inheritdoc/>
        public bool WasLethal { get => this.waslethal; set => this.waslethal = value; }

        /// <inheritdoc/>
        public double Damage { get => this.damage; set => this.damage = value; }

        /// <inheritdoc/>
        public double Angle { get => this.angle;  set => this.angle = value; }

        /// <inheritdoc/>
        public double X { get => this.x; set => this.x = value; }

        /// <inheritdoc/>
        public double Y { get => this.y; set => this.y = value; }

        /// <inheritdoc/>
        public double DX { get => this.dx; set => this.dx = value; }

        /// <inheritdoc/>
        public double DY { get => this.dy; set => this.dy = value; }

        /// <inheritdoc/>
        public void ChangeCoordinatesBySpeed()
        {
            this.x += this.dx;
            this.y += this.dy;
        }
    }
}
