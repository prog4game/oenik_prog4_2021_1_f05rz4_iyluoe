﻿// <copyright file="TeamType.cs" company="Klavaro">
// Copyright (c) Klavaro. All rights reserved.
// </copyright>

namespace Wpf2DShooter.GameModelNS
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Defines the available teams in the game.
    /// </summary>
    public enum TeamType
    {
        /// <summary>
        /// Counter terrorist.
        /// </summary>
        CounterTerrorist,

        /// <summary>
        /// Terrorist.
        /// </summary>
        Terrorist,
    }
}
