﻿// <copyright file="Entity.cs" company="Klavaro">
// Copyright (c) Klavaro. All rights reserved.
// </copyright>

namespace Wpf2DShooter.GameModelNS
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// An <see cref="IEntity"/> implementation.
    /// </summary>
    public class Entity : IEntity
    {
        private ModelType model;
        private IWeapon weapon;
        private bool hasBomb;
        private bool hasBombInHands;
        private double angle;
        private double x;
        private double y;
        private double dx;
        private double dy;
        private short health;
        private TeamType team;

        /// <inheritdoc/>
        public ModelType Model { get => this.model; set => this.model = value; }

        /// <inheritdoc/>
        public IWeapon Weapon { get => this.weapon; set => this.weapon = value; }

        /// <inheritdoc/>
        public bool HasBomb { get => this.hasBomb; set => this.hasBomb = value; }

        /// <inheritdoc/>
        public bool HasBombInHands { get => this.hasBombInHands; set => this.hasBombInHands = value; }

        /// <inheritdoc/>
        public double Angle { get => this.angle; set => this.angle = value; }

        /// <inheritdoc/>
        public double X { get => this.x; set => this.x = value; }

        /// <inheritdoc/>
        public double Y { get => this.y; set => this.y = value; }

        /// <inheritdoc/>
        public double DX { get => this.dx; set => this.dx = value; }

        /// <inheritdoc/>
        public double DY { get => this.dy; set => this.dy = value; }

        /// <inheritdoc/>
        public short Health { get => this.health; set => this.health = value; }

        /// <inheritdoc/>
        public TeamType Team { get => this.team; set => this.team = value; }
    }
}
