﻿// <copyright file="ModelType.cs" company="Klavaro">
// Copyright (c) Klavaro. All rights reserved.
// </copyright>

namespace Wpf2DShooter.GameModelNS
{
    /// <summary>
    /// Describes the different type of skins in the game.
    /// </summary>
    public enum ModelType
    {
        /// <summary>
        /// Because stylecop has nothing better to do.
        /// </summary>
        None,

        /// <summary>
        /// The first skin of the terrorist team.
        /// </summary>
        TerroristSkin1,

        /// <summary>
        /// The second skin of the terrorist team.
        /// </summary>
        TerroristSkin2,

        /// <summary>
        /// The third skin of the terrorist team.
        /// </summary>
        TerroristSkin3,

        /// <summary>
        /// The fourth skin of the terrorist team.
        /// </summary>
        TerroristSkin4,

        /// <summary>
        /// The second skin of the counter terrorist team.
        /// </summary>
        CounterTerroristSkin1,

        /// <summary>
        /// The third skin of the counter terrorist team.
        /// </summary>
        CounterTerroristSkin2,

        /// <summary>
        /// The fourth skin of the counter terrorist team.
        /// </summary>
        CounterTerroristSkin3,

        /// <summary>
        /// The fifth skin of the counter terrorist team.
        /// </summary>
        CounterTerroristSkin4,
    }
}