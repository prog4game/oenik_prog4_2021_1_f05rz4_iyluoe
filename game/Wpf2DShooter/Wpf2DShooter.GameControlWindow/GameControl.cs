﻿// <copyright file="GameControl.cs" company="Klavaro">
// Copyright (c) Klavaro. All rights reserved.
// </copyright>

namespace Wpf2DShooter.GameControlWindow
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Timers;
    using System.Windows;
    using System.Windows.Input;
    using System.Windows.Media;
    using System.Windows.Threading;
    using Wpf2DShooter.GameLogicNS;
    using Wpf2DShooter.GameModelNS;
    using Wpf2DShooter.GameRendererNS;

    /// <summary>
    /// Provides funcionality for controlling the game.
    /// </summary>
    public class GameControl : FrameworkElement, IDisposable
    {
        private IGameLogic logic;
        private GameRenderer renderer;
        private IGameModel model;
        private Timer timer;
        private bool isDisposed;
        private Window window;

        private double lastAngle;
        private GameSoundEffectsPlayer gameSoundEffectsPlayer;

        private bool alreadyHandled;
        private int sumOfWonRounds;

        /// <summary>
        /// Initializes a new instance of the <see cref="GameControl"/> class.
        /// </summary>
        public GameControl()
        {
            this.Loaded += this.GameControlLoaded;
        }

        /// <inheritdoc/>
        public void Dispose()
        {
            this.Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Dispose() calls Dispose(true).
        /// </summary>
        /// <param name="disposing">Local parameter.</param>
        protected virtual void Dispose(bool disposing)
        {
            if (this.isDisposed)
            {
                return;
            }

            if (disposing)
            {
                this.timer.Dispose();
                this.gameSoundEffectsPlayer.Dispose();
                this.logic.Dispose();
            }

            this.isDisposed = true;
        }

        /// <inheritdoc/>
        protected override void OnRender(DrawingContext drawingContext)
        {
            if (this.logic != null && drawingContext != null)
            {
                if (true /* game has not ended yet*/)
                {
                    if (this.renderer != null)
                    {
                        this.alreadyHandled = true;
                        drawingContext.DrawDrawing(this.renderer.BuildDrawing());
                        this.alreadyHandled = false;
                    }
                }
                else
                {
                    // todo : game's ended you wanna play again?
                }
            }
        }

        private void GameControlLoaded(object sender, RoutedEventArgs e)
        {
            ChoseSideWindow choseSideWindow = new ChoseSideWindow();
            var returnVal = choseSideWindow.ShowDialog();
            choseSideWindow = null;
            TeamType chosenTeam = (returnVal ?? false) ? TeamType.CounterTerrorist : TeamType.Terrorist;

            // TeamType chosenTeam = TeamType.Terrorist;
            this.model = new GameModel(this.ActualWidth, this.ActualHeight);
            this.model.ChosenTeam = chosenTeam;
            this.logic = new GameLogic(this.model);
            this.gameSoundEffectsPlayer = new GameSoundEffectsPlayer();
            this.logic.AddSoundEventHandler(this.gameSoundEffectsPlayer.PlayOrStopSound);
            this.renderer = new GameRenderer(this.logic);

            this.timer = new Timer(20);
            this.timer.Elapsed += this.HandleVisualUpdates;

            Mouse.AddMouseWheelHandler(this, this.HandleMouseWheel);

            this.window = Window.GetWindow(this);
            if (this.window != null)
            {
                this.timer.Start();
                this.window.Closing += this.Window_Closing;
            }

            this.InvalidateVisual();
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            this.Dispose();
        }

        private void HandleVisualUpdates(object sender, ElapsedEventArgs e)
        {
            if (this.alreadyHandled)
            {
                return;
            }

            this.alreadyHandled = true;

            this.logic.MoveBots();

            this.logic.HandleWeaponCoolDowns();

            int newSum = this.logic.Model.NumberOfWonRoundsByCounterTerrorists + this.logic.Model.NumberOfWonRoundsByTerrorists;
            if (newSum > this.sumOfWonRounds)
            {
                this.sumOfWonRounds = newSum;

                // this.renderer.ClearBullets();
            }

            this.Dispatcher.Invoke(() =>
            {
                if (this.logic.Model.Player.Health > 0)
                {
                    double angle = this.CalculateAngle();

                    if (this.lastAngle != angle)
                    {
                        this.logic.Model.Player.Angle = angle;
                        this.lastAngle = angle;
                    }

                    if (Mouse.LeftButton == MouseButtonState.Pressed)
                    {
                        this.logic.HandleAttack(this.logic.Model.Player);
                    }
                    else if (this.logic.Model.Player.HasBomb)
                    {
                        this.logic.Model.Bomb.FinishedArming(false);
                        if (this.logic.Model.Bomb.JustStoppedArming)
                        {
                            this.gameSoundEffectsPlayer.PlayOrStopSound(null, new GameSoundEffectsEventArgs() { SoundEffect = SoundEffectType.BombArming, StopSound = true });
                            this.logic.Model.Bomb.JustStoppedArming = false;
                        }
                    }

                    byte wasAbleToMove = 0;

                    if (Keyboard.IsKeyDown(Key.W))
                    {
                        wasAbleToMove += Convert.ToByte(this.logic.SetPlayerVelocity(Direction.Up));
                    }

                    if (Keyboard.IsKeyDown(Key.A))
                    {
                        wasAbleToMove += Convert.ToByte(this.logic.SetPlayerVelocity(Direction.Left));
                    }

                    if (Keyboard.IsKeyDown(Key.S))
                    {
                        wasAbleToMove += Convert.ToByte(this.logic.SetPlayerVelocity(Direction.Down));
                    }

                    if (Keyboard.IsKeyDown(Key.D))
                    {
                        wasAbleToMove += Convert.ToByte(this.logic.SetPlayerVelocity(Direction.Right));
                    }

                    if (wasAbleToMove == 1)
                    {
                        this.logic.UpdateEntityPos(this.logic.Model.Player);
                    }
                }

                this.InvalidateVisual();
            });

            this.alreadyHandled = false;
        }

        private void HandleMouseWheel(object sender, MouseEventArgs e)
        {
            this.Dispatcher.Invoke(() =>
            {
                if (this.logic.SwitchToBomb(this.logic.Model.Player))
                {
                    this.InvalidateVisual();
                }
            });
        }

        private double CalculateAngle()
        {
            Point mousePos = Mouse.GetPosition(this.window);
            Point center = this.renderer.CenterOfScreen();
            center.X += this.model.TileSizeWidth / 2;
            center.Y += this.model.TileSizeHeight / 2;
            return 90 + (Math.Atan2(mousePos.Y - center.Y, mousePos.X - center.X) * 180 / Math.PI);
        }
    }
}
