﻿// <copyright file="GameSoundEffectsPlayer.cs" company="Klavaro">
// Copyright (c) Klavaro. All rights reserved.
// </copyright>

namespace Wpf2DShooter.GameControlWindow
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Reflection;
    using NAudio.Wave;
    using Wpf2DShooter.GameLogicNS;

    /// <summary>
    /// Does the sound effects for the game.
    /// </summary>
    internal class GameSoundEffectsPlayer : IDisposable
    {
        private AudioPlaybackEngine audioPlaybackEngine;
        private Dictionary<byte, CachedSound> cachedSounds;

        /// <summary>
        /// Initializes a new instance of the <see cref="GameSoundEffectsPlayer"/> class.
        /// </summary>
        public GameSoundEffectsPlayer()
        {
            this.audioPlaybackEngine = new AudioPlaybackEngine();
            this.cachedSounds = new Dictionary<byte, CachedSound>();
        }

        /// <inheritdoc/>
        public void Dispose()
        {
            this.audioPlaybackEngine.Dispose();
        }

        /// <summary>
        /// Event handler for playing or stopping a sound effect.
        /// </summary>
        /// <param name="sender">The sender who fired the event.</param>
        /// <param name="args">Data containing which sound should be played.</param>
        internal void PlayOrStopSound(object sender, GameSoundEffectsEventArgs args)
        {
            if (args == null)
            {
                throw new ArgumentNullException(nameof(args), "Args shouldn't be null");
            }

            byte key = (byte)args.SoundEffect;

            if (!this.cachedSounds.ContainsKey(key))
            {
                string filePath = $"Wpf2DShooter.GameControlWindow.Sounds.{Enum.GetName(args.SoundEffect)}.wav";
                Stream stream = Assembly.GetExecutingAssembly().GetManifestResourceStream(filePath);

                this.cachedSounds[key] = new CachedSound(stream);
            }

            if (args.StopSound)
            {
                this.audioPlaybackEngine.StopSound(this.cachedSounds[key]);
            }
            else
            {
                this.audioPlaybackEngine.PlaySound(this.cachedSounds[key]);
            }
        }
    }
}
