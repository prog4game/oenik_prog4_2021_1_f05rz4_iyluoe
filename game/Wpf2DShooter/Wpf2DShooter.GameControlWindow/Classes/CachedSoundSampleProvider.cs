﻿// <copyright file="CachedSoundSampleProvider.cs" company="Klavaro">
// Copyright (c) Klavaro. All rights reserved.
// </copyright>

namespace Wpf2DShooter.GameControlWindow
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using NAudio.Wave;

    /// <summary>
    /// Works as an <see cref="ISampleProvider"/>.
    /// </summary>
    internal class CachedSoundSampleProvider : ISampleProvider
    {
        private readonly CachedSound cachedSound;
        private long position;

        /// <summary>
        /// Initializes a new instance of the <see cref="CachedSoundSampleProvider"/> class.
        /// </summary>
        /// <param name="cachedSound">A copy of <see cref="CachedSound"/>.</param>
        public CachedSoundSampleProvider(CachedSound cachedSound)
        {
            this.cachedSound = cachedSound;
        }

        /// <inheritdoc/>
        public WaveFormat WaveFormat { get => this.cachedSound.WaveFormat; }

        /// <inheritdoc/>
        public int Read(float[] buffer, int offset, int count)
        {
            var availableSamples = this.cachedSound.AudioData.Length - this.position;
            var samplesToCopy = Math.Min(availableSamples, count);
            Array.Copy(this.cachedSound.AudioData, this.position, buffer, offset, samplesToCopy);
            this.position += samplesToCopy;
            return (int)samplesToCopy;
        }
    }
}
