﻿// <copyright file="CachedSound.cs" company="Klavaro">
// Copyright (c) Klavaro. All rights reserved.
// </copyright>

namespace Wpf2DShooter.GameControlWindow
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using NAudio.Wave;

    /// <summary>
    /// Used by AudioPlaybackEngine.
    /// </summary>
    internal class CachedSound
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CachedSound"/> class.
        /// </summary>
        /// <param name="stream">An instance of <see cref="Stream"/> to be read from.</param>
        public CachedSound(Stream stream)
        {
            using (var waveFileReader = new WaveFileReader(stream))
            {
                this.WaveFormat = waveFileReader.WaveFormat;
                var wholeFile = new List<float>((int)(waveFileReader.Length / 2));
                float[] readBuffer;
                while ((readBuffer = waveFileReader.ReadNextSampleFrame()) != null)
                {
                    wholeFile.AddRange(readBuffer);
                }

                this.AudioData = wholeFile.ToArray();
            }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="CachedSound"/> class.
        /// </summary>
        /// <param name="audioFileName">A filepath to the audio file to be read.</param>
        public CachedSound(string audioFileName)
        {
            using (var audioFileReader = new AudioFileReader(audioFileName))
            {
                // TODO: could add resampling in here if required
                this.WaveFormat = audioFileReader.WaveFormat;
                var wholeFile = new List<float>((int)(audioFileReader.Length / 2));
                var readBuffer = new float[audioFileReader.WaveFormat.SampleRate * audioFileReader.WaveFormat.Channels];
                int samplesRead;
                while ((samplesRead = audioFileReader.Read(readBuffer, 0, readBuffer.Length)) > 0)
                {
                    wholeFile.AddRange(readBuffer.Take(samplesRead));
                }

                this.AudioData = wholeFile.ToArray();
            }
        }

        /// <summary>
        /// Gets : An array of floats representing the audio file's data.
        /// </summary>
        public float[] AudioData { get; private set; }

        /// <summary>
        /// Gets : The output WaveFormat of this sample provider.
        /// </summary>
        public WaveFormat WaveFormat { get; private set; }
    }
}
