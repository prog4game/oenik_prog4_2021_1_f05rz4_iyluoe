﻿// <copyright file="AudioPlaybackEngine.cs" company="Klavaro">
// Copyright (c) Klavaro. All rights reserved.
// </copyright>

namespace Wpf2DShooter.GameControlWindow
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using NAudio.Wave;
    using NAudio.Wave.SampleProviders;

    /// <summary>
    /// Does the playing of a sound effect.
    /// </summary>
    internal class AudioPlaybackEngine : IDisposable
    {
        private readonly IWavePlayer outputDevice;
        private readonly MixingSampleProvider mixer;

        /// <summary>
        /// Initializes a new instance of the <see cref="AudioPlaybackEngine"/> class.
        /// </summary>
        /// <param name="sampleRate">Sample rate.</param>
        /// <param name="channelCount">Number of channels.</param>
        public AudioPlaybackEngine(int sampleRate = 44100, int channelCount = 1)
        {
            this.outputDevice = new WaveOutEvent();
            this.mixer = new MixingSampleProvider(WaveFormat.CreateIeeeFloatWaveFormat(sampleRate, channelCount));
            this.mixer.ReadFully = true;
            this.outputDevice.Init(this.mixer);
            this.outputDevice.Play();
        }

        /// <inheritdoc/>
        public void Dispose()
        {
            this.outputDevice.Dispose();
        }

        /// <summary>
        /// Plays an <see cref="CachedSound"/>.
        /// </summary>
        /// <param name="sound"><see cref="CachedSound"/> to be played.</param>
        public void PlaySound(CachedSound sound)
        {
            this.AddMixerInput(new CachedSoundSampleProvider(sound));
        }

        /// <summary>
        /// Stops an <see cref="CachedSound"/>.
        /// </summary>
        /// <param name="sound"><see cref="CachedSound"/> to be stopped.</param>
        public void StopSound(CachedSound sound)
        {
            this.RemoveMixerInput(new CachedSoundSampleProvider(sound));
        }

        private void AddMixerInput(ISampleProvider input)
        {
            this.mixer.AddMixerInput(input);
        }

        private void RemoveMixerInput(ISampleProvider input)
        {
            var toRemove = this.mixer.MixerInputs.Where(x => x.WaveFormat == input.WaveFormat).FirstOrDefault();
            if (toRemove != null)
            {
                this.mixer.RemoveMixerInput(toRemove);
            }
        }
    }
}
