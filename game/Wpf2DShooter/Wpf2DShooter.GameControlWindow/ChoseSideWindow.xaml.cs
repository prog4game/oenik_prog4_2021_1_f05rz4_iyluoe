﻿// <copyright file="ChoseSideWindow.xaml.cs" company="Klavaro">
// Copyright (c) Klavaro. All rights reserved.
// </copyright>

namespace Wpf2DShooter.GameControlWindow
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Data;
    using System.Windows.Documents;
    using System.Windows.Input;
    using System.Windows.Media;
    using System.Windows.Media.Imaging;
    using System.Windows.Shapes;

    /// <summary>
    /// Interaction logic for ChoseSideWindow.xaml.
    /// </summary>
    public partial class ChoseSideWindow : Window
    {
        private bool closedFromClicking;
        private Brush defaultBrush;
        private Brush terroristBrush;
        private Brush counterTerroristBrush;

        /// <summary>
        /// Initializes a new instance of the <see cref="ChoseSideWindow"/> class.
        /// </summary>
        public ChoseSideWindow()
        {
            this.InitializeComponent();
            this.ChangeBackground(this.DefaultBrush);
        }

        private Brush DefaultBrush
        {
            get
            {
                if (this.defaultBrush == null)
                {
                    this.defaultBrush = LoadBrush("Wpf2DShooter.GameControlWindow.Images.choseside.png");
                }

                return this.defaultBrush;
            }
        }

        private Brush TerroristBrush
        {
            get
            {
                if (this.terroristBrush == null)
                {
                    this.terroristBrush = LoadBrush("Wpf2DShooter.GameControlWindow.Images.chosesidet.png");
                }

                return this.terroristBrush;
            }
        }

        private Brush CounterTerroristBrush
        {
            get
            {
                if (this.counterTerroristBrush == null)
                {
                    this.counterTerroristBrush = LoadBrush("Wpf2DShooter.GameControlWindow.Images.chosesidect.png");
                }

                return this.counterTerroristBrush;
            }
        }

        private static Brush LoadBrush(string path)
        {
            BitmapImage bmp = new BitmapImage();
            bmp.BeginInit();
            bmp.StreamSource = Assembly.GetEntryAssembly().GetManifestResourceStream(path);
            bmp.EndInit();
            return new ImageBrush(bmp);
        }

        private void ChangeBackground(Brush brush)
        {
            this.container.Background = brush;
        }

        private void TerroristViewbox_MouseEnter(object sender, MouseEventArgs e)
        {
            this.ChangeBackground(this.TerroristBrush);
        }

        private void CounterTerroristViewbox_MouseEnter(object sender, MouseEventArgs e)
        {
            this.ChangeBackground(this.CounterTerroristBrush);
        }

        private void TerroristViewbox_MouseLeave(object sender, MouseEventArgs e)
        {
            this.ChangeBackground(this.DefaultBrush);
        }

        private void CounterTerroristViewbox_MouseLeave(object sender, MouseEventArgs e)
        {
            this.ChangeBackground(this.DefaultBrush);
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (!this.closedFromClicking)
            {
                Environment.Exit(0);
            }
        }

        private void TerroristViewbox_PreviewMouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            e.Handled = true;
            this.closedFromClicking = true;
            this.DialogResult = false;
            this.Close();
        }

        private void CounterTerroristViewbox_PreviewMouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            e.Handled = true;
            this.closedFromClicking = true;
            this.DialogResult = true;
            this.Close();
        }
    }
}
